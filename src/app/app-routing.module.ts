import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppMainComponent } from './app-main/app-main.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ListEnseignantsComponent } from './list-enseignants/list-enseignants.component';
import { ListMatieresComponent } from './list-matieres/list-matieres.component';
import { ListSessionComponent } from './list-session/list-session.component';
import { ListPromotionComponent } from './list-promotion/list-promotion.component';
import { ListSessionpromotionComponent } from './list-sessionpromotion/list-sessionpromotion.component';
import { ProgrammeCoursComponent } from './programme-cours/programme-cours.component';
import { ListTypeappreciationComponent } from './list-typeappreciation/list-typeappreciation.component';
import { ListEtudiantsComponent } from './list-etudiants/list-etudiants.component';

import { ListEvaluationsComponent } from './list-evaluations/list-evaluations.component';
import { ListNoteEvaluationComponent } from './list-note-evaluation/list-note-evaluation.component';

import { ListAppartenirpromotionComponent } from './list-appartenirpromotion/list-appartenirpromotion.component';
import { ReleveComponent } from './releve/releve.component';

const routes: Routes = [
  {
    path: "",
    component: LoginComponent
  },
  {
    path: "main",
    component: AppMainComponent,
    children: [
      { path: 'main', component: DashboardComponent },
      { path: 'liste/enseignants', component: ListEnseignantsComponent },
      { path: 'liste/etudiants', component: ListEtudiantsComponent },
      { path: 'liste/matieres', component: ListMatieresComponent },
      { path: 'liste/sessions', component: ListSessionComponent },
      { path: 'liste/promotions', component: ListPromotionComponent },
      { path: 'liste/sessionpromotions', component: ListSessionpromotionComponent },
      { path: 'liste/programmecours', component: ProgrammeCoursComponent },
      { path: 'liste/typeappreciations', component: ListTypeappreciationComponent },
      { path: 'liste/etudiants', component: ListEtudiantsComponent },
      { path: 'liste/evaluations', component: ListEvaluationsComponent },
      { path: 'liste/note-evaluations', component: ListNoteEvaluationComponent },
      { path: 'liste/appartenir-promotion', component: ListAppartenirpromotionComponent },
      {path: 'liste/releves', component:ReleveComponent},
    ]
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
