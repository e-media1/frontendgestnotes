import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ConfirmationService, LazyLoadEvent, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { BreadcrumbService } from '../breadcrumb.service';
import { Etudiant } from '../model/etudiant.model';
import { Promotion } from '../model/promotion.model';
import { Appartenirpromotion } from '../model/appartenirpromotion.model';
import { AppartenirpromotionService } from '../services/appartenirpromotion.service';
import { FormService } from '../services/form.service';
import { EtudiantService } from '../services/etudiant.service';
import { PromotionService } from '../services/promotion.service';
 //import { SessionService } from '../services/session.service';
//import { Session } from '../model/session.model';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from 'src/app/services/notification.service';
@Component({
  selector: 'app-list-appartenirpromotion',
  templateUrl: './list-appartenirpromotion.component.html',
  styleUrls: ['./list-appartenirpromotion.component.css'],
  styles: [`
    @media screen and (max-width: 960px) {
        :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:last-child {
            text-align: center;
        }
        :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:nth-child(6) {
            display: flex;
        }
    }
    `],
  providers: [MessageService, ConfirmationService]
})
export class ListAppartenirpromotionComponent implements OnInit {
  appartenirpromotions: Appartenirpromotion[];
  appartenirpromotion: Appartenirpromotion;
  selectedAppartenirpromotions: Appartenirpromotion[];
  selectedEtudiants: Etudiant[];
  etudiant: Etudiant;
  selectedPromotions: Promotion[];
  promotion: Promotion;
  activityValues: number[] = [0, 100];
  submitted: boolean;
  appartenirpromotionDialog: boolean;
  cols: any[];
  @ViewChild('dt') dt: Table;
  totalRecords: any;
  loading: boolean;
  editMode: boolean = false;
  params = {};
  etudiants: Etudiant[];
  promotions: Promotion[];
  //sessions: Session[];
  genres: any[];
  http: any;
  constructor(private appartenirpromotionService: AppartenirpromotionService, private httpClient: HttpClient,
    private etudiantService: EtudiantService,
    private promotionService: PromotionService,
    private notificationService: NotificationService,
    private formService: FormService, private breadcrumbService: BreadcrumbService, private messageService: MessageService, private confirmationService: ConfirmationService) {
    this.breadcrumbService.setItems([
      { label: 'Appartenir promotions' },
      { label: 'appartenir promotions', routerLink: ['liste/appartenir-promotion'] }
    ]);
  }
  ngOnInit(): void {
    this.loading = false;
    this.etudiantService.getEtudiants().subscribe(data => {
      if (data) {
        this.etudiants = data;
      }
    });
    this.promotionService.getPromotions().subscribe(data => {
      if (data) {
        this.promotions = data;
      }
    });
    // this.sessionService.getSessions().subscribe(data => {
    //   if (data) {
    //     this.sessions = data;
    //   }
    // });

  }


  loadAppartenirpromotions(event: LazyLoadEvent) {
    this.loading = true;
    // let page = event.first / event.rows;
    // let size = event.rows;
    this.params = {
      sort: event.sortField, direction: (event.sortOrder == 1 ? "ASC" : "DESC"),
      motcle: (event.globalFilter ? event.globalFilter : null)
    }; this.appartenirpromotionService.getAppartenirpromotion().subscribe(data => {
      if (data) {
        this.appartenirpromotions = data;
        this.loading = false;
      } else {
        this.notificationService.errorMessage(null, null);
        console.error();
      }
    });
    this.loading = false;
  }
  openNew() {
    this.appartenirpromotion = new Appartenirpromotion;
    this.submitted = false;
    this.appartenirpromotionDialog = true;
    this.editMode = true;
  }
  checkAdmin(event) {
  }
  view(appartenirpromotion: Appartenirpromotion, editMode = false) {
    this.appartenirpromotion = appartenirpromotion;
    this.editMode = editMode;
    this.appartenirpromotionDialog = true;
  }
  edit(appartenirpromotion: Appartenirpromotion) {
    this.appartenirpromotion = { ...appartenirpromotion };
    this.appartenirpromotionDialog = true;
  }
  hideDialog() {
    this.appartenirpromotionDialog = false;
    this.submitted = false;
    this.editMode = false;
    this.appartenirpromotion = null;
  }
  setEditMode(mode: boolean) {
    this.editMode = mode;
  }

  delete(appartenirpromotion: Appartenirpromotion) {
    this.confirmationService.confirm({
      message: 'Voulez-vous supprimer ' + appartenirpromotion.etudiant.nomEtudiant + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.appartenirpromotions = this.appartenirpromotions.filter(val => val.id !== appartenirpromotion.id);
        this.appartenirpromotionService.deleteAppartenirpromotion(appartenirpromotion.id);
        this.notificationService.successMessage(null, null);
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Eléments supprimés', life: 3000 });
      }
    });
  }

  update(form: NgForm) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      this.appartenirpromotionService.updateAppartenirpromotion(this.appartenirpromotion.id, this.appartenirpromotion);
      this.notificationService.successMessage(null, null);
      this.appartenirpromotionDialog = false;
    } else {
      this.notificationService.errorMessage(null, null);
    }
  }

  refreshTable() {
    this.dt.reset();
  }

  save(form: NgForm, dq: Appartenirpromotion) {
// this.appartenirpromotion.etudiant.id;
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      if (this.appartenirpromotion.dateExclusion <= Date()) {
        if (this.appartenirpromotion.dateEntree <= Date()) {
          if (this.appartenirpromotion.dateEntree <= this.appartenirpromotion.dateExclusion){
            this.appartenirpromotionService.saveAppartenirpromotion(this.appartenirpromotion).subscribe(appartenirpromotion => {
              if (appartenirpromotion) {
                this.notificationService.successMessage(null, null);
                this.refreshTable();
              } else {
                this.notificationService.errorMessage(null, null);
              }
            });
          }
      }else {
        alert("Revérifiez la date d'entrée'");
      }
    }else {
      alert("Revérifiez la date d'exclusion'");
    }
      this.appartenirpromotionDialog = false;
    }
  }

}

