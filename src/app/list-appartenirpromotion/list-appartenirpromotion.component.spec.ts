import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAppartenirpromotionComponent } from './list-appartenirpromotion.component';

describe('ListAppartenirpromotionComponent', () => {
  let component: ListAppartenirpromotionComponent;
  let fixture: ComponentFixture<ListAppartenirpromotionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListAppartenirpromotionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAppartenirpromotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
