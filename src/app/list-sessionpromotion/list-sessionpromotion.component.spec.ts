import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSessionpromotionComponent } from './list-sessionpromotion.component';

describe('ListSessionpromotionComponent', () => {
  let component: ListSessionpromotionComponent;
  let fixture: ComponentFixture<ListSessionpromotionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListSessionpromotionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSessionpromotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
