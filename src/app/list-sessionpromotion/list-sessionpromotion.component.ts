import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ConfirmationService, LazyLoadEvent, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { BreadcrumbService } from '../breadcrumb.service';
import { Promotion } from '../model/promotion.model';
import { Session } from '../model/session.model';
import { Sessionpromotion } from '../model/sessionpromotion.model';
import { FormService } from '../services/form.service';
import { NotificationService } from '../services/notification.service';
import { PromotionService } from '../services/promotion.service';
import { SessionService } from '../services/session.service';
import { SessionpromotionService } from '../services/sessionpromotion.service';
@Component({
  selector: 'app-list-sessionpromotion',
  templateUrl: './list-sessionpromotion.component.html',
  styleUrls: ['./list-sessionpromotion.component.css'],
  styles: [`
  @media screen and (max-width: 960px) {
      :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:last-child {
          text-align: center;
      }
      :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:nth-child(6) {
          display: flex;
      }
  }
  `],
  providers: [MessageService, ConfirmationService]
})
export class ListSessionpromotionComponent implements OnInit {
  sessionpromotions: Sessionpromotion[];
  sessionpromotion: Sessionpromotion;
  selectedSessionpromotions: Sessionpromotion[];
  activityValues: number[] = [0, 100];
  submitted: boolean;
  sessionpromotionDialog: boolean;
  cols: any[];
  @ViewChild('dt') dt: Table;
  totalRecords: any;
  loading: boolean;
  editMode: boolean = false;
  params = {};


  sessions: Session[];
  promotions: Promotion[];
  constructor(private sessionpromotionService: SessionpromotionService,
    private notificationService: NotificationService, private sessionService: SessionService,
    private promotionService: PromotionService, private breadcrumbService: BreadcrumbService,
    private messageService: MessageService,
    private formService: FormService,
    private confirmationService: ConfirmationService) {
    this.breadcrumbService.setItems([
      { label: 'Sessions-Promotions' },
      { label: 'Liste des Sessions promotions', routerLink: ['liste/sessionpromotions'] }
    ]);
  }
  ngOnInit(): void {
    this.promotionService.getPromotions().subscribe(data => {
      if (data) {
        this.promotions = data;
      }
    });
    this.sessionService.getSessions().subscribe(data => {
      if (data) {
        this.sessions = data;
      }
    });
  }
  loadSessions(event: LazyLoadEvent) {
    this.loading = true;
    // let page = event.first / event.rows;
    // let size = event.rows;
    this.params = {
      sort: event.sortField, direction: (event.sortOrder == 1 ? "ASC" : "DESC"),
      motcle: (event.globalFilter ? event.globalFilter : null)
    }; this.sessionpromotionService.getSessionpromotions().subscribe(data => {
      if (data) {
        this.sessionpromotions = data;
        this.loading = false;
      } else {
        this.notificationService.errorMessage(null, null);
        console.error();
      }
    });
    this.loading = false;
  }
  openNew() {
    this.sessionpromotion = new Sessionpromotion;
    this.submitted = false;
    this.sessionpromotionDialog = true;
    this.editMode = true;
  }
  refreshTable() {
    this.dt.reset();
  }
  checkAdmin(event) {
  }
  view(sessionpromotion: Sessionpromotion, editMode = false) {
    this.sessionpromotion = sessionpromotion;
    this.editMode = editMode;
    this.sessionpromotionDialog = true;
  }
  edit(sessionpromotion: Sessionpromotion) {
    this.sessionpromotion = { ...sessionpromotion };
    this.sessionpromotionDialog = true;
  }
  hideDialog() {
    this.sessionpromotionDialog = false;
    this.submitted = false;
    this.editMode = false;
    this.sessionpromotion = null;
  }
  setEditMode(mode: boolean) {
    this.editMode = mode;
  }
  
  save(form: NgForm, data: Sessionpromotion) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      this.sessionpromotionService.saveSessionpromotion(this.sessionpromotion).subscribe(sessionpromotion => {
        if (sessionpromotion && sessionpromotion.id) {
          this.notificationService.successMessage(null, null);
          this.refreshTable();
        } else {
          this.notificationService.serverError();
        }
      });

      this.sessionpromotionDialog = false;
    }
  }
delete(sessionpromotion: Sessionpromotion) {
  this.confirmationService.confirm({
    message: 'Etes-vous sûr que vous voulez supprimer ' + sessionpromotion.rang + '?',
    header: 'Confirmation',
    icon: 'pi pi-exclamation-triangle',
    accept: () => {
      this.sessionpromotions = this.sessionpromotions.filter(val => val.id !== sessionpromotion.id)
      this.sessionpromotionService.deleteSessionpromotion(sessionpromotion.id)
     //  session = {};
      this.notificationService.successMessage(null, null);
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Session-Promotions supprimés avec succes', life: 3000 });

    }
  });}





  deleteSelectedSessions() {
    this.confirmationService.confirm({
      message: 'Voulez-vous supprimer les sessions sélectionnés?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.sessionpromotions = this.sessionpromotions.filter(val => !this.selectedSessionpromotions.includes(val));
        this.selectedSessionpromotions = null;
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Session-Promotions supprimés', life: 3000 });
      }
    });
  }



  update(form: NgForm) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      this.sessionpromotionService.updateSessionpromotion(this.sessionpromotion.id, this.sessionpromotion);
      this.sessionpromotionDialog = false;
    }
  }











}


