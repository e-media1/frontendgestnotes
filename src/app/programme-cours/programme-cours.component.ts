import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ConfirmationService, LazyLoadEvent } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { BreadcrumbService } from 'src/app/breadcrumb.service';
import { FormService } from 'src/app/services/form.service';
import { NotificationService } from 'src/app/services/notification.service';
import { Enseignant } from '../model/enseignant.model';
import { Matiere } from '../model/matiere.model';
import { ProgrammeCours } from '../model/programmeCours.model';
import { Sessionpromotion } from '../model/sessionpromotion.model';
import { EnseignantService } from '../services/enseignant.service';
import { MatiereService } from '../services/matiere.service';
import { ProgrammeCoursService } from '../services/programme-cours.service';
import { SessionpromotionService } from '../services/sessionpromotion.service';
@Component({
  selector: 'app-programme-cours',
  templateUrl: './programme-cours.component.html',
  styleUrls: ['./programme-cours.component.css'],
  providers: [MessageService, ConfirmationService]
})
export class ProgrammeCoursComponent implements OnInit {

  programmeDialog: boolean;

  totalRecords: number;

  matieres: Matiere[];
  matiere: Matiere;
  selectedMatieres: Matiere[];

  enseignants: Enseignant[];
  enseignant: Enseignant;
  selectedEnseignants: Enseignant[];

  sessionpromotions: Sessionpromotion[];
  sessionpromotion: Sessionpromotion;
  selectedSessionPromos: Sessionpromotion[];

  submitted: boolean;

  cols: any[];

  loading: boolean = false;


  programmes: ProgrammeCours[];

  programme: ProgrammeCours;

  selectedProgrammeCours: ProgrammeCours[];
  editMode: boolean = false;

  text: string;
  params = {};

  @ViewChild('dt') dt: Table;


  constructor(private matiereService: MatiereService, private messageService: MessageService,
    private confirmationService: ConfirmationService, private breadcrumbService: BreadcrumbService,
    private programmecoursService: ProgrammeCoursService, private notificationService: NotificationService,
    private sessionpromotionsService: SessionpromotionService, private formService: FormService, private enseignantService: EnseignantService,) {
    this.breadcrumbService.setItems([

      { label: 'Programme de cours', routerLink: ['liste/programmecours'] }
    ]);
  }

  ngOnInit(): void {

    this.matiereService.getMatieres().subscribe(data => {
      if (data) {
        this.matieres = data;
      }
    });

    this.enseignantService.getEnseignants().subscribe(data => {
      if (data) {
        this.enseignants = data;
      }
    });
    this.sessionpromotionsService.getSessionpromotions().subscribe(data => {
      if (data) {
        this.sessionpromotions = data;
      }
    })
  }

  loadProgrammeCours(event: LazyLoadEvent) {
    this.loading = true;
    // this.params = {
    //   sort: event.sortField, direction: (event.sortOrder == 1 ? "ASC" : "DESC"),
    //   motcle: (event.globalFilter ? event.globalFilter : null)
    // };
    this.programmecoursService.findAll().subscribe(data => {
      if (data) {
        this.programmes = data;
        this.loading = false;
      } else {
        this.notificationService.errorMessage(null, null);
      }
    });
    this.loading = false;
  }

  refreshTable() {
    this.dt.reset();
  }

  openNew() {
    this.programme = new ProgrammeCours;
    this.submitted = false;
    this.programmeDialog = true;
    this.editMode = true;
  }

  view(programme: ProgrammeCours, editMode = false) {
    this.programme = programme;
    this.editMode = editMode;
    this.programmeDialog = true;
  }

  deleteSelectedProgrammes() {
    this.confirmationService.confirm({
      message: 'Voulez-vous supprimer les sessions sélectionnés?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.programmes = this.programmes.filter(val => !this.selectedProgrammeCours.includes(val));
        this.selectedProgrammeCours = null;
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Cours supprimés', life: 3000 });
      }
    });
  }

  hideDialog() {
    this.programmeDialog = false;
    this.submitted = false;
    this.editMode = false;
    this.programme = null;
  }
  setEditMode(mode: boolean) {
    this.editMode = mode;
  }

  save(form: NgForm, data: ProgrammeCours) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      if (this.programme.dateDebutCours < this.programme.dateFinCours){
        this.programmecoursService.saveProgramme(this.programme).subscribe(programme => {
          if (programme && programme.id) {
            this.notificationService.successMessage(null, null);
            this.refreshTable();
          } else {
            this.notificationService.serverError();
          }
        });
      }else {
        alert("Les dates sont incorrectes");
      }

      this.programmeDialog = false;
    }
  }


  delete(programme: ProgrammeCours) {
    this.confirmationService.confirm({
      message: 'Etes-vous sûr que vous voulez supprimer ?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.programmes = this.programmes.filter(val => val.id !== programme.id);
        this.programmecoursService.deleteProgramme(programme.id);
        this.notificationService.successMessage(null, null);
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Programme cours supprimés avec succes', life: 3000 });
      }
    });
  }


  update(form: NgForm) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      if (this.programme.dateDebutCours < this.programme.dateFinCours){
      this.programmecoursService.updateProgramme(this.programme.id, this.programme);
      this.notificationService.successMessage(null, null);
      this.programmeDialog = false;
    }else {
      alert("Les dates sont incorrectes");
    }
  }else {
    this.notificationService.errorMessage(null, null);
  }
  }
}
