import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgrammeCoursComponent } from './programme-cours.component';

describe('ProgrammeCoursComponent', () => {
  let component: ProgrammeCoursComponent;
  let fixture: ComponentFixture<ProgrammeCoursComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgrammeCoursComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammeCoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
