import { Component, OnInit } from '@angular/core';
import { AppMainComponent } from '../app-main/app-main.component';

@Component({
  selector: 'app-menu',
  templateUrl: './app-menu.component.html',
  styleUrls: ['./app-menu.component.css']
})
export class AppMenuComponent implements OnInit {

  model: any[];

  constructor(public appMain: AppMainComponent) { }

  ngOnInit() {

    this.model = [
      { label: 'Tableau de bord', icon: 'pi pi-fw pi-home', routerLink: ['/main'] },
      {
        label: 'Enseignant', routerLink: ['/uikit'],
        items: [
          { label: 'Liste des enseignants', icon: 'pi pi-fw pi-table', routerLink: ['liste/enseignants'] },

        ]
      },
      {

        label: 'Etudiants', routerLink: ['/uikit'],
        items: [
          { label: 'Liste des étudiants', icon: 'pi pi-fw pi-table', routerLink: ['liste/etudiants'] },

        ]
      },

      {
        label: 'Session', routerLink: ['/uikit'],
        items: [
          { label: 'Liste des sessions', icon: 'pi pi-fw pi-table', routerLink: ['liste/sessions'] },

        ]
      },
      {
        label: 'Matiere', routerLink: ['/uikit'],
        items: [
          { label: 'Liste des matieres', icon: 'pi pi-fw pi-table', routerLink: ['liste/matieres'] },

        ]
      },

      {
        label: 'Promotion', routerLink: ['/uikit'],
        items: [
          { label: 'Liste des promotions', icon: 'pi pi-fw pi-table', routerLink: ['liste/promotions'] },

        ]

      },


      {
        label: 'Session-Promotion', routerLink: ['/uikit'],
        items: [
          { label: 'Liste des sessions Promotions', icon: 'pi pi-fw pi-table', routerLink: ['liste/sessionpromotions'] },

        ]
      },
      {
        label: 'Programme cours', routerLink: ['/uikit'],
        items: [
          { label: 'Liste des programmes de cours', icon: 'pi pi-fw pi-table', routerLink: ['liste/programmecours'] },

        ]
      },

      {
        label: 'Type Appreciations', routerLink: ['/uikit'],
        items: [
          { label: 'Liste des Types Appreciations', icon: 'pi pi-fw pi-table', routerLink: ['liste/typeappreciations'] },

        ]
      },
      {
        label: 'Evaluations', routerLink: ['/uikit'],
        items: [
          { label: 'Liste des evaluations', icon: 'pi pi-fw pi-table', routerLink: ['liste/evaluations'] },

        ]
      },
       {
        label: 'Notes evalutions ', routerLink: ['/uikit'],
        items: [
          { label: 'Liste des notes d\'evaluations', icon: 'pi pi-fw pi-table', routerLink: ['liste/note-evaluations'] },
        ]

      },
       {
        label: 'Appartenir Promotions ', routerLink: ['/uikit'],
        items: [
          { label: 'Appartenir promotion', icon: 'pi pi-fw pi-table', routerLink: ['liste/appartenir-promotion'] },
        ]

        },
        {
          label: 'Releves ', routerLink: ['/uikit'],
          items: [
            { label: 'Liste des releves', icon: 'pi pi-fw pi-table', routerLink: ['liste/releves'] },
          ]
          }

    ];

  }

 onMenuClick() {
    this.appMain.menuClick = true;
  }

}
