import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ConfirmationService, LazyLoadEvent, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { BreadcrumbService } from '../breadcrumb.service';
import { Session } from '../model/session.model';
import { FormService } from '../services/form.service';
import { NotificationService } from '../services/notification.service';
import { SessionService } from '../services/session.service';

@Component({
  selector: 'app-list-session',
  templateUrl: './list-session.component.html',
  styleUrls: ['./list-session.component.css'],
  styles: [`
  @media screen and (max-width: 960px) {
      :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:last-child {
          text-align: center;
      }


      :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:nth-child(6) {
          display: flex;
      }
  }

  `],
  providers: [MessageService, ConfirmationService]
})
export class ListSessionComponent implements OnInit {
  sessions: Session[];
  session: Session;
  selectedSessions: Session[];
  activityValues: number[] = [0, 100];
  submitted: boolean;
  SessionDialog: boolean;
  cols: any[];
  @ViewChild('dt') dt: Table;
  totalRecords: any;
  loading: boolean;

  editMode: boolean = false;
  params = {};


  constructor(private sessionService: SessionService,
    private notificationService: NotificationService,
    private breadcrumbService: BreadcrumbService,
    private messageService: MessageService,
    private formService: FormService,
    private confirmationService: ConfirmationService) {
    this.breadcrumbService.setItems([
      { label: 'Sessions' },
      { label: 'Liste des Sessions', routerLink: ['liste/sessions'] }

    ]);
  }
  ngOnInit(): void {
  }
  loadSessions(event: LazyLoadEvent) {
    this.loading = true;
    // let page = event.first / event.rows;
    // let size = event.rows;
    this.params = {
      sort: event.sortField, direction: (event.sortOrder == 1 ? "ASC" : "DESC"),
      motcle: (event.globalFilter ? event.globalFilter : null)
    }; this.sessionService.getSessions().subscribe(data => {
      if (data) {
        this.sessions = data;
        this.loading = false;
      } else {
        this.notificationService.errorMessage(null, null);
        console.error();
      }
    });
    this.loading = false;
  }

  openNew() {
    this.session = new Session;
    this.submitted = false;
    this.SessionDialog = true;
    this.editMode = true;
  }

  refreshTable() {
    this.dt.reset();
  }

  checkAdmin(event) {

  }

  view(session: Session, editMode = false) {
    this.session = session;
    this.editMode = editMode;
    this.SessionDialog = true;
  }

  edit(session: Session) {
    this.session = { ...session };
    this.SessionDialog = true;
  }


  hideDialog() {
    this.SessionDialog = false;
    this.submitted = false;
    this.editMode = false;
    this.session = null;
  }
  setEditMode(mode: boolean) {
    this.editMode = mode;
  }




  save(form: NgForm, data: Session) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      if (this.session.dateFinsession <= Date()) {
        if (this.session.dateDebutsession <= Date()) {
          if (this.session.dateDebutsession < this.session.dateFinsession) {
            this.sessionService.saveSession(this.session).subscribe(session => {
              if (session && session.id) {
                this.notificationService.successMessage(null, null);
                this.refreshTable();
              } else {
                this.notificationService.serverError();
              }
            });
          } else {
            alert("Les dates sont incorrectes");
          }
        } else {
          alert("Vérifiez la date de debut de session");
        }
      }
      else {
        alert("Vérifiez la date de fin de session");
      }
      this.SessionDialog = false;
    }
  }
  delete(session: Session) {
    this.confirmationService.confirm({
      message: 'Etes-vous sûr que vous voulez supprimer ' + session.rangSession + '?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.sessions = this.sessions.filter(val => val.id !== session.id);
        this.sessionService.deleteSession(session.id);
        this.notificationService.successMessage(null, null);
        alert("Mise a jour effectuer avec succes");
      }

    });

    this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Session supprimés avec succes', life: 3000 });

  }


  update(form: NgForm) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      if (this.session.dateFinsession <= Date()) {
        if (this.session.dateDebutsession <= Date()) {
          if (this.session.dateDebutsession < this.session.dateFinsession) {
            this.sessionService.updateSession(this.session.id, this.session);
            this.notificationService.successMessage(null, null);
            this.SessionDialog = false;
          }
          else {
            alert("Les dates sont incorrectes");
          }
        } else {
          alert("Vérifiez la date de debut de session");
        }
      } else {
        alert("Vérifiez la date de fin de session");
      }

    }
    this.SessionDialog = false;

  }



}
