
import { Promotion } from "./promotion.model";
import { Session } from "./session.model";

export class Sessionpromotion{
    public id ?:number;
    public rang ?:String;
    public session:Session;
    public promotion:Promotion;
}
