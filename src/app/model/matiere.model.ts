import { Enseignant } from "./enseignant.model";
import { Session } from "./session.model";

export class Matiere {
  public id?: number;
  public codeMatiere?: number;
  public libelleMatiere?: string;
  public coefMatiere?: number;
  public dateDebutMatiere?: string;
  public dateFinMatiere?: string;
  public enseignant?: Enseignant;
  public session?: Session;
}
