declare var jQuery:any;
declare var $ : any;

export class Helpers {

	// supprimer les champs de params qui sont null, undefined, ""
	static cleanHttpParams(params : any) {
		let cleanedHttpParams = {};
		Object.keys(params).forEach(param => {
			if (params[param] != null) {
				 cleanedHttpParams [param] = params[param];
			}
		});
		return cleanedHttpParams;
	}


}
