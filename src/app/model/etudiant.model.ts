export class Etudiant{
  public id ?:number;
  public matricule ?:number;
  public nomEtudiant?: string;
  public prenomEtudiant?: string;
  public sexeEtudiant?: string;
  public dateNaissanceEtudiant?: string;
  public lieuNaissanceEtudiant?: string;
  public nationaliteEtudiant?: string;
  public adresseEtudiant?:string;
  public emailEtudiant?:string;
  public telephoneEtudiant?: string;
  public villeEtudiant?: string;
  public paysEtudiant?: string;
  public numeroCNI?: string;
  public dateInscription?: string;
}
