import { Etudiant } from "./etudiant.model";
import { Evaluation } from "./evaluation.model";
export class NoteEvaluation {
  public id: number;
  public noteEtudiant: number;
  public etudiant:Etudiant;
  public evaluation:Evaluation

}

