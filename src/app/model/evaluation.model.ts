import { ProgrammeCours } from "./programmeCours.model";
export class Evaluation{
  public id?:number;
  public dateEvaluation?: string;
  public titreEvaluation?: string;
  public programmeCours?:ProgrammeCours;
}
