import { Etudiant } from "./etudiant.model";
import { Promotion } from "./promotion.model";

export class Appartenirpromotion {
  public id?:number;
  public dateEntree?: string;
  public exclu?: number;
  public motifExclusion?: string;
  public dateExclusion?: string;
  public dernierSession?: string;
  public etudiant?: Etudiant;
  public promotion?: Promotion;


}
