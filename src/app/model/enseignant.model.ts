export class Enseignant{
		id?:number;
		matricule?:number;
		nom?:string;
		prenom?:string;
		email?:string;
		telephone?:string;
		ville?:string;
		pays?:string;
		adresse?:string;
		date_recrut?:string;
}
