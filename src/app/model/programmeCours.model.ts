import { Enseignant } from "./enseignant.model";
import { Matiere } from "./matiere.model";
import { Sessionpromotion } from "./sessionpromotion.model";

export class ProgrammeCours {
  id?: number;
  coef?: number;
  observationCours?: string;
  dateDebutCours?: string;
  dateFinCours?: string;
  matiere?: Matiere;
  enseignant?: Enseignant;
  sessionpromotion?: Sessionpromotion;
}
