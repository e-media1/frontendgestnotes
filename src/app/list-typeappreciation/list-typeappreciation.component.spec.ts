import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTypeappreciationComponent } from './list-typeappreciation.component';

describe('ListTypeappreciationComponent', () => {
  let component: ListTypeappreciationComponent;
  let fixture: ComponentFixture<ListTypeappreciationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListTypeappreciationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTypeappreciationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
