import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ConfirmationService, LazyLoadEvent, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { BreadcrumbService } from '../breadcrumb.service';
import { Typeappreciation } from '../model/typeappreciation.model';
import { FormService } from '../services/form.service';
import { NotificationService } from '../services/notification.service';
import { TypeappreciationService } from '../services/typeappreciation.service';

@Component({
  selector: 'app-list-typeappreciation',
  templateUrl: './list-typeappreciation.component.html',
  styleUrls: ['./list-typeappreciation.component.css'],
  styles: [`
  @media screen and (max-width: 960px) {
      :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:last-child {
          text-align: center;
      }


      :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:nth-child(6) {
          display: flex;
      }
  }

  `],
  providers: [MessageService, ConfirmationService]

})
export class ListTypeappreciationComponent implements OnInit {
  typeappreciations:Typeappreciation[];
  typeappreciation:Typeappreciation;
  selectedTypeappreciations:Typeappreciation[];
  activityValues: number[] = [0, 100];
  submitted: boolean;
  typeappreciationDialog: boolean;
  cols: any[];
  @ViewChild('dt') dt: Table;
  totalRecords: any;
  loading: boolean;
  editMode: boolean = false;
  params = {};

  constructor(private typeappreciationService:TypeappreciationService, private notificationService: NotificationService, private breadcrumbService: BreadcrumbService,
    private messageService: MessageService,
    private formService: FormService,
    private confirmationService: ConfirmationService) {
    this.breadcrumbService.setItems([
      { label: 'Typeappreciations' },
      {label: 'Liste des types appreciations', routerLink: ['liste/typeappreciations']}

  ]);
   }
  ngOnInit(): void {
  }
  loadTypeappreciations(event: LazyLoadEvent) {
    this.loading = true;
    // let page = event.first / event.rows;
    // let size = event.rows;
    this.params = {
      sort: event.sortField, direction: (event.sortOrder == 1 ? "ASC" : "DESC"),
      motcle: (event.globalFilter ? event.globalFilter : null)
    };   this.typeappreciationService.getTypeappreciations().subscribe(data => {
      if (data) {
        this.typeappreciations = data;
        this.loading = false;
      } else {
        this.notificationService.errorMessage(null, null);
        console.error();
      }
    });
    this.loading = false;
  }

  openNew() {
    this.typeappreciation = new Typeappreciation;
    this.submitted = false;
    this.typeappreciationDialog = true;
    this.editMode = true;
      }

      refreshTable() {
        this.dt.reset();
      }

      checkAdmin(event){

      }

      view(typeappreciation: Typeappreciation, editMode = false) {
        this.typeappreciation = typeappreciation;
        this.editMode = editMode;
        this.typeappreciationDialog = true;
      }

      edit(typeappreciation: Typeappreciation) {
        this.typeappreciation = { ...typeappreciation };
        this.typeappreciationDialog = true;
      }

hideDialog() {
  this.typeappreciationDialog = false;
  this.submitted = false;
  this.editMode = false;
  this.typeappreciation = null;
}
setEditMode(mode: boolean) {
  this.editMode = mode;
}



save(form: NgForm, data:Typeappreciation) {
  this.submitted = true;
  if (this.formService.isNgFormValid(form)) {
    this.typeappreciationService.saveTypeappreciation(this.typeappreciation).subscribe(typeappreciation => {
      if (typeappreciation && typeappreciation.id) {
        this.notificationService.successMessage(null, null);
        this.refreshTable();
      } else {
        this.notificationService.serverError();
      }
    });
    this.typeappreciationDialog = false;
  }
}




delete(typeappreciation: Typeappreciation) {
  this.confirmationService.confirm({
    message: 'Etes-vous sûr que vous voulez supprimer ?',
    header: 'Confirmation',
    icon: 'pi pi-exclamation-triangle',
    accept: () => {
      this.typeappreciations = this.typeappreciations.filter(val => val.id !== typeappreciation.id);
      this.typeappreciationService.deleteTypeappreciation(typeappreciation.id);
      this.notificationService.successMessage(null, null);
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Programme cours supprimés avec succes', life: 3000 });
    }
  });
}

deleteSelectedSessions() {

  this.confirmationService.confirm({
      message: 'Voulez-vous supprimer les sessions sélectionnés?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
          this.typeappreciations = this.typeappreciations.filter(val => !this.selectedTypeappreciations.includes(val));
          this.selectedTypeappreciations = null;
          this.messageService.add({severity: 'success', summary: 'Successful', detail: 'Session supprimés', life: 3000});

        }
  });
}


update(form: NgForm) {
  this.submitted = true;
  if (this.formService.isNgFormValid(form)) {
    this.typeappreciationService.updateTypeappreciation(this.typeappreciation.id, this.typeappreciation);
    this.notificationService.successMessage(null, null);
    this.typeappreciationDialog = false;
  } else {
    this.notificationService.errorMessage(null, null);
  }
}

}
