import { Component, OnInit, ViewChild } from '@angular/core';
import { BreadcrumbService } from '../breadcrumb.service';
import { Enseignant } from '../model/enseignant.model';
import { ConfirmationService, LazyLoadEvent } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { EnseignantService } from '../services/enseignant.service';
import { NotificationService } from '../services/notification.service';
import { Table } from 'primeng/table';
import { NgForm } from '@angular/forms';
import { FormService } from '../services/form.service';

@Component({
  selector: 'app-list-enseignants',
  templateUrl: './list-enseignants.component.html',
  styleUrls: ['./list-enseignants.component.scss'],
  providers: [MessageService, ConfirmationService]
})
export class ListEnseignantsComponent implements OnInit {


  enseignants: Enseignant[];
  enseignant: Enseignant;
  selectedEnseignants: Enseignant[];
  activityValues: number[] = [0, 100];
  totalRecords: number;
  loading: boolean = false;
  editMode: boolean = false;

  submitted: boolean;
  enseignantDialog: boolean;
  cols: any[];
  params = {};
  @ViewChild('dt') dt: Table;

  constructor(private enseignantService: EnseignantService,
    private breadcrumbService: BreadcrumbService,
    private messageService: MessageService, private formService: FormService, private notificationService: NotificationService,
    private confirmationService: ConfirmationService) {
    this.breadcrumbService.setItems([
      { label: 'Enseignants' },
      { label: 'Liste des enseignants', routerLink: ['liste/enseignants'] }

    ]);
  }

  ngOnInit(): void {

  }

  loadEnseignants(event: LazyLoadEvent) {
    this.loading = true;

    this.params = {
      sort: event.sortField, direction: (event.sortOrder == 1 ? "ASC" : "DESC"),
      motcle: (event.globalFilter ? event.globalFilter : null)
    };


    this.enseignantService.getEnseignants().subscribe(data => {
      if (data) {
        this.enseignants = data;
        // this.totalRecords = data.totalElements;
        this.loading = false;
      } else {
        this.notificationService.errorMessage(null, null);
      }
    });
    this.loading = false;
  }

  openNew() {
    this.enseignant = new Enseignant;
    this.submitted = false;
    this.enseignantDialog = true;
    this.editMode = true;
  }
  view(enseignant: Enseignant, editMode = false) {
    this.enseignant = enseignant;
    this.editMode = editMode;
    this.enseignantDialog = true;
  }


  edit(enseignant: Enseignant) {
    this.enseignant = { ...enseignant };
    this.enseignantDialog = true;
  }

  deleteSelectedEnseignants() {
    this.confirmationService.confirm({
      message: 'Voulez-vous supprimer les enseignants sélectionnés?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.enseignants = this.enseignants.filter(val => !this.selectedEnseignants.includes(val));
        this.selectedEnseignants = null;
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Enseignants supprimés', life: 3000 });
      }
    });
  }

  delete(enseignant: Enseignant) {
    this.confirmationService.confirm({
      message: 'Voulez-vous supprimer ' + enseignant.nom + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.enseignants = this.enseignants.filter(val => val.id !== enseignant.id);
        this.enseignantService.deleteEnseignant(enseignant.id);
        this.notificationService.successMessage(null, null);
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Enseignant supprimé', life: 3000 });

      }
    });
  }


  hideDialog() {
    this.enseignantDialog = false;
    this.submitted = false;
    this.editMode = false;
    this.enseignant = null;
  }

  setEditMode(mode: boolean) {
    this.editMode = mode;
  }


  refreshTable() {
    this.dt.reset();
  }

  save(form: NgForm, data: Enseignant) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      if (this.enseignant.date_recrut <= Date()) {
        this.enseignantService.saveEnseignant(this.enseignant).subscribe(enseignant => {
          if (enseignant && enseignant.id) {
            this.notificationService.successMessage(null, null);
            this.refreshTable();
          } else {
            this.notificationService.serverError();
          }
        });
      } else {
        alert("Revérifiez la date de recrutement");
      }
      this.enseignantDialog = false;
    }
  }

  update(form: NgForm) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      if (this.enseignant.date_recrut <= Date()) {
        this.enseignantService.updateEnseignant(this.enseignant.id, this.enseignant);
        this.notificationService.successMessage(null, null);
        this.enseignantDialog = false;
      }else {
        alert("Revérifiez la date de recrutement");
      }
      
    } else {
      this.notificationService.errorMessage(null, null);
    }
  }

}


