import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ConfirmationService, LazyLoadEvent, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { BreadcrumbService } from '../breadcrumb.service';
import { Enseignant } from '../model/enseignant.model';
import { Matiere } from '../model/matiere.model';
import { MatiereService } from '../services/matiere.service';
import { FormService } from '../services/form.service';
import { EnseignantService } from '../services/enseignant.service';
import { SessionService } from '../services/session.service';
import { Session } from '../model/session.model';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from 'src/app/services/notification.service';
@Component({
  selector: 'app-list-matieres',
  templateUrl: './list-matieres.component.html',
  styleUrls: ['./list-matieres.component.css'],
  styles: [`
    @media screen and (max-width: 960px) {
        :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:last-child {
            text-align: center;
        }
        :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:nth-child(6) {
            display: flex;
        }
    }
    `],
  providers: [MessageService, ConfirmationService]
})
export class ListMatieresComponent implements OnInit {
  matieres: Matiere[];
  matiere: Matiere;
  selectedMatieres: Matiere[];
  selectedEnseignants: Enseignant[];
  activityValues: number[] = [0, 100];
  submitted: boolean;
  matiereDialog: boolean;
  cols: any[];
  @ViewChild('dt') dt: Table;
  totalRecords: any;
  loading: boolean;
  editMode: boolean = false;
  params = {};
  enseignants: Enseignant[];
  sessions: Session[];
  http: any;
  constructor(private matiereService: MatiereService, private httpClient: HttpClient,
    private enseignantService: EnseignantService,
    private sessionService: SessionService, private notificationService: NotificationService,
    private formService: FormService, private breadcrumbService: BreadcrumbService, private messageService: MessageService, private confirmationService: ConfirmationService) {
    this.breadcrumbService.setItems([
      { label: 'Matieres' },
      { label: 'Liste des matieres', routerLink: ['liste/matieres'] }
    ]);
  }
  ngOnInit(): void {
    this.enseignantService.getEnseignants().subscribe(data => {
      if (data) {
        this.enseignants = data;
      }
    });
    this.sessionService.getSessions().subscribe(data => {
      if (data) {
        this.sessions = data;
      }
    });
  }


  loadMatieres(event: LazyLoadEvent) {
    this.loading = true;
    // let page = event.first / event.rows;
    // let size = event.rows;
    this.params = {
      sort: event.sortField, direction: (event.sortOrder == 1 ? "ASC" : "DESC"),
      motcle: (event.globalFilter ? event.globalFilter : null)
    }; this.matiereService.getMatieres().subscribe(data => {
      if (data) {
        this.matieres = data;
        this.loading = false;
      } else {
        this.notificationService.errorMessage(null, null);
        console.error();
      }
    });
    this.loading = false;
  }
  openNew() {
    this.matiere = new Matiere;
    this.submitted = false;
    this.matiereDialog = true;
    this.editMode = true;
  }
  checkAdmin(event) {
  }
  view(matiere: Matiere, editMode = false) {
    this.matiere = matiere;
    this.editMode = editMode;
    this.matiereDialog = true;
  }
  edit(matiere: Matiere) {
    this.matiere = { ...matiere };
    this.matiereDialog = true;
  }
  hideDialog() {
    this.matiereDialog = false;
    this.submitted = false;
    this.editMode = false;
    this.matiere = null;
  }
  setEditMode(mode: boolean) {
    this.editMode = mode;
  }
  delete(matiere: Matiere) {
    this.confirmationService.confirm({
      message: 'Voulez-vous supprimer ' + matiere.libelleMatiere + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.matieres = this.matieres.filter(val => val.id !== matiere.id);
        this.matiereService.deleteMatiere(matiere.id);
        this.notificationService.successMessage(null, null);
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Matières supprimés', life: 3000 });
      }
    });
  }


  refreshTable() {
    this.dt.reset();
  }

  save(form: NgForm, data: Matiere) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      if (this.matiere.dateFinMatiere <= Date()) {
        if (this.matiere.dateDebutMatiere <= Date()) {
          if (this.matiere.dateDebutMatiere < this.matiere.dateFinMatiere) {
            this.matiereService.saveMatiere(this.matiere).subscribe(matiere => {
              if (matiere && matiere.id) {
                this.notificationService.successMessage(null, null);
                this.refreshTable();
              } else {
                this.notificationService.errorMessage(null, null);
              }
            });
          } else {
            alert("Les dates sont incorrectes");
          }
        } else {
          alert("Vérifiez la date de début");
        }

      }
      else {
        alert("Vérifiez la date de fin");
      }
      this.matiereDialog = false;
    }
  }


  update(form: NgForm) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      if (this.matiere.dateFinMatiere <= Date()) {
        if (this.matiere.dateDebutMatiere <= Date()) {
          if (this.matiere.dateDebutMatiere < this.matiere.dateFinMatiere) {
            this.matiereService.updateMatiere(this.matiere.id, this.matiere);
          }
          else {
            alert("Les dates sont incorrectes");
          }
        } else {
          alert("Vérifiez la date de début");
        }
      }else {
        alert("Vérifiez la date de fin");
      }
    }
    this.matiereDialog = false;

  }

}
