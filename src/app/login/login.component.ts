import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  dark: boolean;

  checked: boolean;
  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  OnLogin(){
    this.router.navigateByUrl('/main');
  }

}
