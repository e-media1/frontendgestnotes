import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import html2canvas from 'html2canvas';
import { ConfirmationService, LazyLoadEvent, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { BreadcrumbService } from '../breadcrumb.service';
import { Etudiant } from '../model/etudiant.model';
import { EtudiantService } from '../services/etudiant.service';
import { FormService } from '../services/form.service';
import { GenreService } from '../services/genre.service';
import { NotificationService } from '../services/notification.service';
import * as jspdf from 'jspdf';
@Component({
  selector: 'app-list-etudiants',
  templateUrl: './list-etudiants.component.html',
  styleUrls: ['./list-etudiants.component.css'],
  styles: [`
    @media screen and (max-width: 960px) {
        :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:last-child {
            text-align: center;
        }


        :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:nth-child(6) {
            display: flex;
        }
    }

    `],
  providers: [MessageService, ConfirmationService]
})
export class ListEtudiantsComponent implements OnInit {
  etudiants: Etudiant[];
  etudiant: Etudiant;
  selectedEtudiants: Etudiant[];
  activityValues: number[] = [0, 100];
  submitted: boolean;
  etudiantDialog: boolean;
  cols: any[];
  @ViewChild('dt') dt: Table;
  totalRecords: any;
  loading: boolean;
  editMode: boolean = false;
  params = {};
  genres: any[];
  constructor(private etudiantService: EtudiantService, private genreService: GenreService, private breadcrumbService: BreadcrumbService,
    private messageService: MessageService, private notificationService: NotificationService, private formService: FormService, private confirmationService: ConfirmationService) {
    this.breadcrumbService.setItems([
      { label: 'Etudiants' },
      { label: 'Liste des étudiants', routerLink: ['liste/etudiants'] }

    ]);
  }

  ngOnInit(): void {

    this.genres = this.genreService.list();
  }


  loadEtudiants(event: LazyLoadEvent) {
    this.loading = true;
    // let page = event.first / event.rows;
    // let size = event.rows;
    this.params = {
      sort: event.sortField, direction: (event.sortOrder == 1 ? "ASC" : "DESC"),
      motcle: (event.globalFilter ? event.globalFilter : null)
    }; this.etudiantService.getEtudiants().subscribe(data => {
      if (data) {
        this.etudiants = data;
        this.loading = false;
      } else {
        this.notificationService.errorMessage(null, null);
        console.error();
      }
    });
    this.loading = false;
  }

  openNew() {
    this.etudiant = new Etudiant;
    this.submitted = false;
    this.etudiantDialog = true;
    this.editMode = true;
  }


  checkAdmin(event) {

  }

  view(etudiant: Etudiant, editMode = false) {
    this.etudiant = etudiant;
    this.editMode = editMode;
    this.etudiantDialog = true;
  }

  edit(etudiant: Etudiant) {
    this.etudiant = { ...etudiant };
    this.etudiantDialog = true;
  }

  hideDialog() {
    this.etudiantDialog = false;
    this.submitted = false;
    this.editMode = false;
    this.etudiant = null;
  }
  setEditMode(mode: boolean) {
    this.editMode = mode;
  }





  deleteSelectedEtudiants() {
    this.confirmationService.confirm({
      message: 'Voulez-vous supprimer les étudiants sélectionnés?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.etudiants = this.etudiants.filter(val => !this.selectedEtudiants.includes(val));
        this.selectedEtudiants = null;
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Etudiant supprimé', life: 3000 });
      }
    });
  }

  refreshTable() {
    this.dt.reset();
  }

  save(form: NgForm, data: Etudiant) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      if (this.etudiant.dateNaissanceEtudiant <= Date()) {
        if (this.etudiant.dateInscription <= Date()) {
          this.etudiantService.saveEtudiant(this.etudiant).subscribe(etudiant => {
            if (etudiant && etudiant.id) {
              this.notificationService.successMessage(null, null);
              this.refreshTable();
            } else {
              this.notificationService.serverError();
            }
          });
        } else {
          alert(" Vérifiez la date d'inscription");
        }
      }
      else {
        alert("Vérifiez la date de naissance");
      }
    }
    this.etudiantDialog = false;
  }

  Imprimer() {
    var element = document.getElementById('tablereleve')
    html2canvas(element).then((canvas) => {
      var imgwidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgwidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL(this.etudiantService.host + '/listNoteEvaluation')
      let pdf = new jspdf.jsPDF('p', 'mm', 'a4');
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgwidth, imgHeight);
      pdf.save('Monreleve.pdf')
      console.log(canvas);
    })
  }
  update(form: NgForm) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      if (this.etudiant.dateNaissanceEtudiant <= Date()) {
        if (this.etudiant.dateInscription <= Date()) {
          this.etudiantService.updateEtudiant(this.etudiant.id, this.etudiant);
          this.notificationService.successMessage(null, null);
          this.etudiantDialog = false;
        } else {
          alert("Vérifiez la date d'inscription");
        }
      } else {
        alert("Vérifiez la date de naissance");
      }
    } else {
      this.notificationService.errorMessage(null, null);
    }
  }

  delete(etudiant: Etudiant) {
    this.confirmationService.confirm({
      message: 'Voulez-vous supprimer ' + etudiant.nomEtudiant + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.etudiants = this.etudiants.filter(val => val.id !== etudiant.id);
        this.etudiantService.deleteEtudiant(etudiant.id);
        this.notificationService.successMessage(null, null);
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Promotions supprimés', life: 3000 });
      }
    });
  }


}
