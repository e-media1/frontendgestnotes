import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ConfirmationService, LazyLoadEvent, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { BreadcrumbService } from '../breadcrumb.service';
import { Enseignant } from '../model/enseignant.model';
import { Evaluation } from '../model/evaluation.model';
import { Matiere } from '../model/matiere.model';
import { ProgrammeCours } from '../model/programmeCours.model';
import { EvaluationService } from '../services/evaluation.service';
import { FormService } from '../services/form.service';
import { NotificationService } from 'src/app/services/notification.service';
import { ProgrammeCoursService } from '../services/programme-cours.service';

@Component({
  selector: 'app-evaluation',
  templateUrl: './list-evaluations.component.html',
  styleUrls: ['./list-evaluations.component.css'],

  styles: [`
    @media screen and (max-width: 960px) {
        :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:last-child {
            text-align: center;
        }
        :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:nth-child(6) {
            display: flex;
        }
    }
    `],

  providers: [MessageService, ConfirmationService]
})
export class ListEvaluationsComponent implements OnInit {
  evaluations: Evaluation[];
  evaluation: Evaluation;
  selectedEvaluations: Evaluation[];

  activityValues: number[] = [0, 100];
  submitted: boolean;
  evaluationDialog: boolean;
  cols: any[];
  @ViewChild('dt') dt: Table;
  totalRecords: any;
  loading: boolean = false;


  editMode: boolean = false;
  params = {};

  matiere: Matiere[];

  enseignant: Enseignant[];

  programmes: ProgrammeCours[];
  programme: ProgrammeCours;

  constructor(private evaluationService: EvaluationService, private programmeCoursService: ProgrammeCoursService,
    private notificationService: NotificationService, private breadcrumbService: BreadcrumbService,
    private messageService: MessageService, private formService: FormService, private confirmationService: ConfirmationService) {
    this.breadcrumbService.setItems([
      { label: 'Evaluations' },
      { label: 'Liste des évaluations', routerLink: ['liste/evaluations'] }]);
  }


  ngOnInit(): void {
    this.programmeCoursService.getProgrammes().subscribe(data => {
      if (data) {
        this.programmes = data;
      }
    });
  }

  loadEvaluations(event: LazyLoadEvent) {
    this.loading = true;
    this.evaluationService.getEvaluations().subscribe(data => {
      if (data) {
        this.evaluations = data;
        this.loading = false;
      } else {
        this.notificationService.errorMessage(null, null);
        console.error();
      }
    });
    this.loading = false;
  }
  openNew() {
    this.evaluation = new Evaluation;
    this.submitted = false;
    this.evaluationDialog = true;
    this.editMode = true;
  }
  checkAdmin(event) {
  }
  view(evaluation: Evaluation, editMode = false) {
    this.evaluation = evaluation;
    this.editMode = editMode;
    this.evaluationDialog = true;
  }
  // edit(evaluation: Evaluation) {
  //   this.evaluation = { ...evaluation };
  //   this.evaluationDialog = true;
  // }
  hideDialog() {
    this.evaluationDialog = false;
    this.submitted = false;
    this.editMode = false;
    this.evaluation = null;
  }

  setEditMode(mode: boolean) {
    this.editMode = mode;
  }


  delete(evaluation: Evaluation) {
    this.confirmationService.confirm({
      message: 'Voulez-vous réllement supprimer ' + evaluation.titreEvaluation + '?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.evaluations = this.evaluations.filter(val => val.id !== evaluation.id);
        this.evaluationService.deleteEvaluation(evaluation.id);
        this.notificationService.successMessage(null, null);
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Suppression effectué avec succes', life: 3000 });
      }
    });
  }


  deleteSelectedEvaluations() {
    this.confirmationService.confirm({
      message: 'Voulez-vous supprimer les évaluations supprimées',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.evaluations = this.evaluations.filter(val => !this.selectedEvaluations.includes(val));
        this.selectedEvaluations = null;
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Les évaluations ont été supprimées avec suucès', life: 3000 });
      }
    });
  }

  save(form: NgForm) {

    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      // console.log(this.evaluation);
      this.evaluationService.save(this.evaluation).subscribe(evaluation => {

        if (evaluation && evaluation.id) {
          this.notificationService.successMessage(null, null);
          this.refreshTable();
        } else {
          this.notificationService.serverError();
        }
      });
      this.evaluationDialog = false;
    }
  }
  refreshTable() {
    this.dt.reset();
  }
  update(form: NgForm) {
    this.submitted = true;

    if (this.formService.isNgFormValid(form)) {
      this.evaluationService.updateEvaluation(this.evaluation.id, this.evaluation);
    }
    this.evaluationDialog = false;


  }
}
