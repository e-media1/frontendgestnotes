import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListNoteEvaluationComponent } from './list-note-evaluation.component';

describe('ListNoteEvaluationComponent', () => {
  let component: ListNoteEvaluationComponent;
  let fixture: ComponentFixture<ListNoteEvaluationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListNoteEvaluationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListNoteEvaluationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
