import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ConfirmationService, LazyLoadEvent, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { BreadcrumbService } from '../breadcrumb.service';
import { Enseignant } from '../model/enseignant.model';
import { Etudiant } from '../model/etudiant.model';
import { Evaluation } from '../model/evaluation.model';
import { NoteEvaluation } from '../model/note-evaluation.model';
import { EtudiantService } from '../services/etudiant.service';
import { EvaluationService } from '../services/evaluation.service';
import { FormService } from '../services/form.service';
import { NoteEvaluationService } from '../services/note-evaluation.service';
import { NotificationService } from '../services/notification.service';
@Component({
  selector: 'app-list-note-evaluation',
  templateUrl: './list-note-evaluation.component.html',
  styleUrls: ['./list-note-evaluation.component.css'],
  styles: [`
    @media screen and (max-width: 960px) {
        :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:last-child {
            text-align: center;
        }
        :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:nth-child(6) {
            display: flex;
        }
    }
    `],
  providers: [MessageService, ConfirmationService]
})
export class ListNoteEvaluationComponent implements OnInit {
  noteEvaluations:NoteEvaluation[];
  noteEvaluation:NoteEvaluation;
  selectedNoteEvaluations:NoteEvaluation[];
  activityValues: number[] = [0, 100];
  submitted: boolean;
  noteEvaluationDialog: boolean;
  cols: any[];
  @ViewChild('dt') dt: Table;
  totalRecords: any;
  loading: boolean;
  editMode: boolean = false;
  params = {};
  //enseignants: Enseignant[];

  evaluations:Evaluation[];
  etudiants:  Etudiant[];
  etudiant : Etudiant;
  evaluation : Evaluation;

  constructor(private noteEvaluationService:NoteEvaluationService,
    private evaluationService:EvaluationService,
    private formService: FormService,private notificationService: NotificationService,
    private etudiantService:EtudiantService, private breadcrumbService: BreadcrumbService,private messageService: MessageService,private confirmationService: ConfirmationService) {
    this.breadcrumbService.setItems([


      { label: 'NoteEvaluations' },
      { label: 'Liste des NoteEvaluations', routerLink: ['liste/NoteEvaluations'] }

    ]);
  }

  ngOnInit(): void {

    this.etudiantService.getEtudiants().subscribe(data => {
      if (data) {
        this.etudiants = data;
      }
    });
    this.evaluationService.getEvaluations().subscribe(data => {
      if (data) {
        this.evaluations = data;
      }
    });

  }
  loadNoteEvaluations(event: LazyLoadEvent) {
    this.loading = true;

    this.params = {
      sort: event.sortField, direction: (event.sortOrder == 1 ? "ASC" : "DESC"),
      motcle: (event.globalFilter ? event.globalFilter : null)
    }; this.noteEvaluationService.getNoteEvaluations().subscribe(data => {
      if (data) {
        this.noteEvaluations = data;
        this.loading = false;
      } else {
        this.notificationService.errorMessage(null, null);
        console.error();
      }
    });
    this.loading = false;
  }
  openNew() {
    this.noteEvaluation = new NoteEvaluation;
    this.submitted = false;
    this.noteEvaluationDialog = true;
    this.editMode = true;
  }


  checkAdmin(event) {

  }

  view(NoteEvaluation: NoteEvaluation, editMode = false) {
    this.noteEvaluation = NoteEvaluation;
    this.editMode = editMode;
    this.noteEvaluationDialog = true;
  }

  edit(NoteEvaluation: NoteEvaluation) {
    this.noteEvaluation = { ...NoteEvaluation };
    this.noteEvaluationDialog = true;
  }

  hideDialog() {
    this.noteEvaluationDialog = false;
    this.submitted = false;
    this.editMode = false;
    this.noteEvaluation = null;
  }
  setEditMode(mode: boolean) {
    this.editMode = mode;
  }



  delete(noteEvaluation: NoteEvaluation) {
    this.confirmationService.confirm({
      message: 'Voulez-vous réllement supprimer ? ',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.noteEvaluations = this.noteEvaluations.filter(val => val.id !== noteEvaluation.id);
        this.noteEvaluationService.deleteNoteEvaluation(noteEvaluation.id);
        this.notificationService.successMessage(null, null);
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'NoteEvaluation supprimés avec succes', life: 3000 });
      }
    });
  }


  deleteSelectedNoteEvaluations() {
    this.confirmationService.confirm({
      message: 'Voulez-vous supprimer les NoteEvaluations sélectionnés?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.noteEvaluations = this.noteEvaluations.filter(val => !this.selectedNoteEvaluations.includes(val));
        this.selectedNoteEvaluations = null;

        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'NoteEvaluation supprimés', life: 3000 });
      }
    });
  }



  save(form: NgForm) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      if (this.evaluation.dateEvaluation < Date()){
        this.noteEvaluationService.saveNoteEvaluation(this.noteEvaluation).subscribe(noteEvaluation => {
          if (noteEvaluation && noteEvaluation.id) {
            this.notificationService.successMessage(null, null);
            this.refreshTable();
          } else {
            this.notificationService.errorMessage(null, null);
          }
        });
      }else {
        alert("Revérifiez la date de recrutement");
      }
      this.noteEvaluationDialog = false;
    }
  }

  refreshTable() {
    this.dt.reset();
  }

  update(form: NgForm) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      if (this.evaluation.dateEvaluation < Date()){
      this.noteEvaluationService.updateNoteEvaluation(this.noteEvaluation.id, this.noteEvaluation);
      this.noteEvaluationDialog = false;
    }else {
      alert("Revérifiez la date de recrutement");
    }
    this.noteEvaluationDialog = false;
  }
  }
}
