import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ConfirmationService, LazyLoadEvent, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { BreadcrumbService } from '../breadcrumb.service';
import { Etudiant } from '../model/etudiant.model';
import { Promotion } from '../model/promotion.model';
import { FormService } from '../services/form.service';
import { EtudiantService } from '../services/etudiant.service';
import { PromotionService } from '../services/promotion.service';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from 'src/app/services/notification.service';
@Component({
  selector: 'app-list-promotion',
  templateUrl: './list-promotion.component.html',
  styleUrls: ['./list-promotion.component.css'],
  styles: [`
    @media screen and (max-width: 960px) {
        :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:last-child {
            text-align: center;
        }
        :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:nth-child(6) {
            display: flex;
        }
    }
    `],
  providers: [MessageService, ConfirmationService]
})
export class ListPromotionComponent implements OnInit {

  selectedEtudiants: Etudiant[];
  etudiant: Etudiant;
  selectedPromotions: Promotion[];
  promotion: Promotion;
  activityValues: number[] = [0, 100];
  submitted: boolean;
  promotionDialog: boolean;
  cols: any[];
  @ViewChild('dt') dt: Table;
  totalRecords: any;
  loading: boolean;
  editMode: boolean = false;
  params = {};
  etudiants: Etudiant[];
  promotions: Promotion[];
  //sessions: Session[];
  genres: any[];
  http: any;
  constructor(private httpClient: HttpClient,

    private promotionService: PromotionService,
    private notificationService: NotificationService,
    private formService: FormService, private breadcrumbService: BreadcrumbService, private messageService: MessageService, private confirmationService: ConfirmationService) {
    this.breadcrumbService.setItems([
      { label: 'Promotions' },
      { label: 'Promotions', routerLink: ['liste/promotion'] }
    ]);
  }
  ngOnInit(): void {
    this.loading = false;

  }


  loadPromotions(event: LazyLoadEvent) {
    this.loading = true;
    // let page = event.first / event.rows;
    // let size = event.rows;
    this.params = {
      sort: event.sortField, direction: (event.sortOrder == 1 ? "ASC" : "DESC"),
      motcle: (event.globalFilter ? event.globalFilter : null)
    }; this.promotionService.getPromotions().subscribe(data => {
      if (data) {
        this.promotions = data;
        this.loading = false;
      } else {
        this.notificationService.errorMessage(null, null);
        console.error();
      }
    });
    this.loading = false;
  }
  openNew() {
    this.promotion = new Promotion;
    this.submitted = false;
    this.promotionDialog = true;
    this.editMode = true;
  }
  checkAdmin(event) {
  }
  view(promotion: Promotion, editMode = false) {
    this.promotion = promotion;
    this.editMode = editMode;
    this.promotionDialog = true;
  }
  edit(promotion: Promotion) {
    this.promotion = { ...promotion };
    this.promotionDialog = true;
  }
  hideDialog() {
    this.promotionDialog = false;
    this.submitted = false;
    this.editMode = false;
    this.promotion = null;
  }
  setEditMode(mode: boolean) {
    this.editMode = mode;
  }

  delete(promotion: Promotion) {
    this.confirmationService.confirm({
      message: 'Voulez-vous supprimer ?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.promotions = this.promotions.filter(val => val.id !== promotion.id);
        this.promotionService.deletePromotion(promotion.id);
        this.notificationService.successMessage(null, null);
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Eléments supprimés', life: 3000 });
      }
    });
  }


  refreshTable() {
    this.dt.reset();
  }

  save(form: NgForm, data: Promotion) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      if (this.promotion.date_debut <= Date()) {
        if (this.promotion.date_fin <= Date()) {
          if (this.promotion.date_debut < this.promotion.date_fin) {
            this.promotionService.savePromotion(this.promotion).subscribe(promotion => {
              if (promotion) {
                this.notificationService.successMessage(null, null);
                this.refreshTable();
              } else {
                this.notificationService.errorMessage(null, null);
              }
            });
          } else {
            alert("Les dates sont incorrectes");
          }
        } else {
          alert("Vérifiez la date de fin");
        }
      } else {
        alert("Vérifiez la date de début");
      }
      this.promotionDialog = false;
    }
  }


  update(form: NgForm) {
    this.submitted = true;
    if (this.formService.isNgFormValid(form)) {
      if (this.promotion.date_debut <= Date()) {
        if (this.promotion.date_fin <= Date()) {
          if (this.promotion.date_debut < this.promotion.date_fin) {
            this.promotionService.updatePromotion(this.promotion.id, this.promotion);
          }
          else {
            alert("Les dates sont incorrectes");
          }
        }
        else {
          alert("Vérifiez la date de fin");
        }
      }else {
        alert("Vérifiez la date de début");
      }

      this.promotionDialog = false;
    }
    this.promotionDialog = false;
  }

}

