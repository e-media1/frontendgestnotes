import { Component, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { LazyLoadEvent } from 'primeng/api';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { Etudiant } from '../model/etudiant.model';
import { NoteEvaluation } from '../model/note-evaluation.model';
import { Sessionpromotion } from '../model/sessionpromotion.model';
import { EtudiantService } from '../services/etudiant.service';
import { FormService } from '../services/form.service';
import { NoteEvaluationService } from '../services/note-evaluation.service';
import { NotificationService } from '../services/notification.service';
import { ReleveService } from '../services/releve.service';
import { SessionpromotionService } from '../services/sessionpromotion.service';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { EvaluationService } from '../services/evaluation.service';
import { Evaluation } from '../model/evaluation.model';
import { ProgrammeCours } from '../model/programmeCours.model';
import { ProgrammeCoursService } from '../services/programme-cours.service';
import { Matiere } from '../model/matiere.model';
import { Enseignant } from '../model/enseignant.model';
//import { EventEmitter } from 'stream';
@Component({
  selector: 'app-releve',
  templateUrl: './releve.component.html',
  styleUrls: ['./releve.component.css']
})
export class ReleveComponent implements OnInit {
  public noteEvaluation:NoteEvaluation;
  public noteEvaluations:NoteEvaluation[];

  public etudiant:Etudiant;
  public etudiants:Etudiant[];

  public sessionpromotion:Sessionpromotion;
  public sessionpromotions:Sessionpromotion[];

  public evaluation:Evaluation;
  public evaluations:Evaluation[];

  public programme:ProgrammeCours;
  public programmes:ProgrammeCours[];

  public matieres: Matiere[];
  public matiere: Matiere;
 
  public enseignants: Enseignant[];
  public enseignant: Enseignant;

  public selectReleve:number;
  public selectSess:number=0;
  public selectEtu:number=0;

  public releveNotes:any;

  public loading :boolean;
  public releveterm= new Subject<String>();
  public releveResults:any;
  editMode: boolean = false;
  ReleveDialog: boolean;
  activityValues: number[] = [0, 100];
  submitted: boolean;
  //@Output() searchEvent = new EventEmitterter();

  host:String="http://localhost:8080/";

  params: { //sort: string;
    // direction: string;
      // motcle: any;
      SearchSespromo: any;
      Searchetudiant: any;
     };

  constructor(private noteEvaluationService:NoteEvaluationService,
    private releveService:ReleveService,
    private etudiantService:EtudiantService,
    private programmeCoursService: ProgrammeCoursService,
    private formService: FormService,private notificationService: NotificationService,
    private evaluationService:EvaluationService,
    private sessionpromotionService:SessionpromotionService
    ) { }

  ngOnInit(): void {
    this.etudiantService.getEtudiants().subscribe(data => {
      if (data) {
        this.etudiants = data;
      }
    });
    this.sessionpromotionService.getSessionpromotions().subscribe(data => {
      if (data) {
        this.sessionpromotions = data;
      }
    });
    this.noteEvaluationService.getNoteEvaluations().subscribe(data => {
      if (data) {
        this.noteEvaluations = data;
      }
    });

    this.evaluationService.getEvaluations().subscribe(data => {
      if (data) {
        this.evaluations = data;
      }
    });
    this.programmeCoursService.getProgrammes().subscribe(data => {
      if (data) {
        this.programmes = data;
      }
    });
  }



  loadReleves(event: LazyLoadEvent) {
    this.loading = true;
    this.params = {
      SearchSespromo:(event.globalFilter ? event.globalFilter : null),
        Searchetudiant:(event.globalFilter ? event.globalFilter : null)
    };
     this.noteEvaluationService.getNoteEvaluations().subscribe(data => {
      if (data) {
        this.noteEvaluations = data;
        this.loading = false;
      } else {
        this.notificationService.errorMessage(null, null);
        console.error();
      }
    });
    this.loading = false;
  }

  public releveForm = new FormGroup({
    releve:new FormControl("",Validators.required),
  });

public releve(){
//   this.releveterm.pipe(
//     map((e: any) => {
// console.log(e.target.value);
// return e.target.value
//     });
//     debounceTime(400),
//     distinctUntilChanged(),
//     switchMap(term=>{
//       this.loading = true;
//       return this.releveService._rechercheentrer(term2,term1);
//     })
//   )
}



openNew() {
  this.noteEvaluation = new NoteEvaluation;
  this.submitted = false;
  this.ReleveDialog = true;
  this.editMode = true;
    }


    checkAdmin(event){

    }

    view(NoteEvaluation: NoteEvaluation, editMode = false) {
      this.noteEvaluation = NoteEvaluation;
      this.editMode = editMode;
      this.ReleveDialog = true;
    }

    edit(NoteEvaluation: NoteEvaluation) {
      this.noteEvaluation = { ...NoteEvaluation };
      this.ReleveDialog = true;
    }

hideDialog() {
this.ReleveDialog = false;
this.submitted = false;
this.editMode = false;
this.noteEvaluation = null;
}
setEditMode(mode: boolean) {
this.editMode = mode;
}
curDate=new Date();
Imprimer(){
var element = document.getElementById('tablereleve')
html2canvas(element).then((canvas) =>{
  var imgwidth =208;
var pageHeight = 295;
var imgHeight = canvas.height * imgwidth / canvas.width;
var heightLeft = imgHeight;

const contentDataURL = canvas.toDataURL(this.host+'/listNoteEvaluation')
 let pdf = new jspdf.jsPDF('p','mm','a4');
 var position = 0;
 pdf.addImage(contentDataURL,'PNG',0,position,imgwidth,imgHeight);
 pdf.save('Monreleve.pdf')
console.log(canvas);
})
}

recherche(selectEtu:any,selectSess:any){
  this.submitted = true;
this.releveService.afficher(selectEtu,selectSess).subscribe
(data=>{
  if (data) {
    this.noteEvaluations= data;
    this.loading = false;
    console.log(data);
  } else {
    this.notificationService.errorMessage(null, null);
    console.error();
  }
})
}



search(selectEtu:any){
  this.submitted = true;
this.releveService.affiche(selectEtu).subscribe
(data=>{
  if (data) {
    this.noteEvaluations= data;
    this.loading = false;
    console.log(data);
  } else {
    this.notificationService.errorMessage(null, null);
    console.error();
  }
})
}
 
}
