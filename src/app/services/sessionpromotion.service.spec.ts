import { TestBed } from '@angular/core/testing';

import { SessionpromotionService } from './sessionpromotion.service';

describe('SessionpromotionService', () => {
  let service: SessionpromotionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SessionpromotionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
