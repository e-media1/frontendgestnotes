import { HttpClient,HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Etudiant } from '../model/etudiant.model';
import { Evaluation } from '../model/evaluation.model';
import { NoteEvaluation } from '../model/note-evaluation.model';
import { LogService } from './log.service';

@Injectable({
  providedIn: 'root'
})
export class NoteEvaluationService {
  public host:string="http://localhost:8080";


  constructor(private httpClient:HttpClient, private logService: LogService) { }

  findAll(): Observable<NoteEvaluation> {
    //const cleanedParams = Helpers.cleanHttpParams(params);
    return this.httpClient.get<NoteEvaluation>(`${this.host}/listNoteEvaluation`)
       // params: cleanedParams,
      .pipe(
        catchError(
          (error: any): Observable<NoteEvaluation> => {
            this.logService.error(error);
            return of(null);
          }
        )
      );
  }


afficher(note:NoteEvaluation){
  this.httpClient.get<NoteEvaluation>(`${this.host}/listNoteEvaluation`);
}

  getNoteEvaluations(): Observable<NoteEvaluation[]> {
    return this.httpClient.get<NoteEvaluation[]>(`${this.host}/listNoteEvaluation`)
      .pipe(catchError((error: any): Observable<NoteEvaluation[]> => {
        this.logService.error(error);
        return of(null);
      }));
  }



  public deleteNoteEvaluation(id: number) {
    this.httpClient.delete(`${this.host}/deleteNoteEvaluation/${id}`).subscribe(data => {
      console.log(data);
    })
  }

  public saveNoteEvaluation(noteEvaluation:NoteEvaluation):Observable<NoteEvaluation>{
    // let note= new NoteEvaluation;
    // note.id=noteEvaluation.id;
    // note.noteEtudiant=-noteEvaluation.noteEtudiant;

    // note.evaluation = new Evaluation;
    // note.evaluation.id = noteEvaluation.evaluation.id;
    // note.evaluation.titreEvaluation = noteEvaluation.evaluation.titreEvaluation;
    // note.evaluation.dateEvaluation = noteEvaluation.evaluation.dateEvaluation;
    // note.evaluation.programmeCours = noteEvaluation.evaluation.programmeCours;

    // note.etudiant = new Etudiant;
    // note.etudiant.id=noteEvaluation.etudiant.id;
    // note.etudiant.adresseEtudiant=noteEvaluation.etudiant.adresseEtudiant;
    // note.etudiant.dateInscription=noteEvaluation.etudiant.dateInscription;
    // note.etudiant.dateNaissanceEtudiant=noteEvaluation.etudiant.dateNaissanceEtudiant;
    // note.etudiant.emailEtudiant=noteEvaluation.etudiant.emailEtudiant;
    // note.etudiant.lieuNaissanceEtudiant=noteEvaluation.etudiant.lieuNaissanceEtudiant;
    // note.etudiant.matricule=noteEvaluation.etudiant.matricule;
    // note.etudiant.nationaliteEtudiant=noteEvaluation.etudiant.nationaliteEtudiant;
    // note.etudiant.nomEtudiant=noteEvaluation.etudiant.nomEtudiant;
    // note.etudiant.numeroCNI=noteEvaluation.etudiant.numeroCNI;
    // note.etudiant.paysEtudiant=noteEvaluation.etudiant.paysEtudiant;
    // note.etudiant.prenomEtudiant=noteEvaluation.etudiant.prenomEtudiant;
    // note.etudiant.sexeEtudiant=noteEvaluation.etudiant.sexeEtudiant;
    // note.etudiant.telephoneEtudiant=noteEvaluation.etudiant.telephoneEtudiant;
    // note.etudiant.villeEtudiant=noteEvaluation.etudiant.villeEtudiant;

    return this.httpClient.post<NoteEvaluation>(`${this.host}/saveNoteEvaluation`, noteEvaluation)

    .pipe(catchError((error:any):Observable<NoteEvaluation> =>{
      this.logService.error(error);
      return of (new NoteEvaluation);
    }));
  }


  public getResource(url:any):Observable<NoteEvaluation>{
    return this.httpClient.get<NoteEvaluation>(url);
  }


  public updateNoteEvaluation(id: number, noteEvaluation: NoteEvaluation) {
    this.httpClient.put(`${this.host}/updateNoteEvaluation/${id}`, noteEvaluation).subscribe(data => {
      console.log(data);
    });
  }

}
