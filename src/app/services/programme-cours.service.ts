import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LogService } from './log.service';
import { ProgrammeCours } from '../model/programmeCours.model';
import { Helpers } from '../model/helpers';
import { Matiere } from '../model/matiere.model';
import { Enseignant } from '../model/enseignant.model';
import { Sessionpromotion } from '../model/sessionpromotion.model';
@Injectable({
  providedIn: 'root'
})
export class ProgrammeCoursService {

  public host: string = "http://localhost:8080";

  constructor(private httpClient: HttpClient,
    private logService: LogService) { }

  getProgrammes(): Observable<ProgrammeCours[]> {
    return this.httpClient.get<ProgrammeCours[]>(this.host + "/listProgramme_cour")
      .pipe(catchError((error: any): Observable<ProgrammeCours[]> => {
        this.logService.error(error);
        return of(null);
      }));
  }



  findAll(): Observable<ProgrammeCours[]> {
    // const cleanedParams = Helpers.cleanHttpParams(params);
    return this.httpClient.get<ProgrammeCours[]>(`${this.host}/listProgramme_cour`)
      .pipe(
        catchError(
          (error: any): Observable<ProgrammeCours[]> => {
            this.logService.error(error);
            return of(null);
          }
        )
      );
  }

  saveProgramme(programme: ProgrammeCours): Observable<ProgrammeCours> {

    let p = new ProgrammeCours;
    // p.id=programme.id;
    p.coef = programme.coef;
    p.observationCours = programme.observationCours;
    p.dateDebutCours = programme.dateDebutCours;
    p.dateFinCours = programme.dateFinCours;

    p.matiere = new Matiere;
    p.matiere.id = programme.matiere.id;
    p.matiere.codeMatiere = programme.matiere.codeMatiere;
    p.matiere.coefMatiere = programme.matiere.coefMatiere;
    p.matiere.dateDebutMatiere = programme.matiere.dateDebutMatiere;
    p.matiere.dateFinMatiere = programme.matiere.dateFinMatiere;
    p.matiere.libelleMatiere = programme.matiere.libelleMatiere;

    p.enseignant = new Enseignant;
    p.enseignant.id = programme.enseignant.id;
    p.enseignant.nom = programme.enseignant.nom;
    p.enseignant.prenom = programme.enseignant.prenom;
    p.enseignant.email = programme.enseignant.email;
    p.enseignant.matricule = programme.enseignant.matricule;
    p.enseignant.adresse = programme.enseignant.adresse;
    p.enseignant.date_recrut = programme.enseignant.date_recrut;
    p.enseignant.pays = programme.enseignant.pays;
    p.enseignant.telephone = programme.enseignant.telephone;
    p.enseignant.ville = programme.enseignant.ville;

    p.sessionpromotion = new Sessionpromotion;
    p.sessionpromotion.id = programme.sessionpromotion.id;
    p.sessionpromotion.rang = programme.sessionpromotion.rang;

    return this.httpClient.post<ProgrammeCours>(this.host + "/saveProgrammeCour", programme)
      .pipe(catchError((error: any): Observable<ProgrammeCours> => {
        this.logService.error(error);
        return of(new ProgrammeCours);
      }));
  }


  public deleteProgramme(id: number) {
    this.httpClient.delete(`${this.host}/deleteProgrammeCour/${id}`).subscribe(data => {
      console.log(data);
    });
  }

  public updateProgramme(id: number, programme:ProgrammeCours) {

    this.httpClient.put(`${this.host}/updateProgrammeCour/${id}`, programme).subscribe(data => {
      console.log(data);
    });
  }

  // get(id: string): Observable<ProgrammeCours> {
  //   return this.httpClient.get<ProgrammeCours>(this.host+"/listProgramme_cour/{id}")
  //     .pipe(catchError((error: any): Observable<ProgrammeCours> => {
  //       this.logService.error(error);
  //       return of(new ProgrammeCours);
  //     }));
  // }
}
