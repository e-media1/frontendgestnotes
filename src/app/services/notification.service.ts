import { Injectable } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';



@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  constructor(private messageService: MessageService, private confirmationService: ConfirmationService) { }

  serverError() {
    this.errorMessage(null, null);
  }

  errorMessage(summary: string, message: string) {
    if (summary == null) {
      summary = "Une erreur est survenue";
    }
    if (message == null) {
      message = "Veuillez conctacter l'administrateur!";
    }
    this.messageService.add({
      severity: "error",
      summary: summary,
      detail: message,
      life: 5000,
    });
  }

  warningMessage(summary: string, message: string) {
    this.messageService.add({
      severity: "warning",
      summary: summary,
      detail: message,
      life: 5000,
    });
  }

  infoMessage(summary: string, message: string) {
    this.messageService.add({
      severity: "info",
      summary: summary,
      detail: message,
      life: 5000,
    });
  }

  successMessage(summary: string, message: string) {
    if (summary == null) {
      summary = "Opération réussie!";
    }
    if (message == null) {
      message = "";
    }
    this.messageService.add({
      severity: "success",
      summary: summary,
      detail: message,
      life: 5000,
    });
  }

  deleteConfirm(acceptCallback: Function) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected products?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => acceptCallback
    });
  }

}
