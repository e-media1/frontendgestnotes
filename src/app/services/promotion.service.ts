import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Promotion } from '../model/promotion.model';
import { Observable, of } from 'rxjs';
import { LogService } from './log.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PromotionService {

  public host: string = "http://localhost:8080";

  constructor(private httpClient: HttpClient, private logService: LogService) { }

  public getPromotions(): Observable<Promotion[]> {
    return this.httpClient.get<Promotion[]>(this.host + "/listPromotions")
      .pipe(catchError((error: any): Observable<Promotion[]> => {
        this.logService.error(error);
        return of(null);
      }));
  }

  public getPromotionByKeyword(mc: string) {
    return this.httpClient.get(this.host + "listPromotions/byName?mc=" + mc);
  }


  public savePromotion(promotion: Promotion): Observable<Promotion> {

    return this.httpClient.post<Promotion>(this.host + "/savePromotion", promotion)
      .pipe(catchError((error: any): Observable<Promotion> => {
        this.logService.error(error);
        return of(new Promotion);
      }));
  }

  public getResource(url: any): Observable<Promotion> {
    return this.httpClient.get<Promotion>(url);
  }



  public deletePromotion(id:number){
    this.httpClient.delete(`${this.host}/deletePromotion/${id}`).subscribe(data => {
      console.log(data);
    })
  }

  public updatePromotion(id: number, promotion:Promotion) {

    this.httpClient.put(`${this.host}/updatePromotion/${id}`, promotion).subscribe(data => {

      console.log(data);
    });
  }

}
