import { TestBed } from '@angular/core/testing';

import { AppartenirpromotionService } from './appartenirpromotion.service';

describe('AppartenirpromotionService', () => {
  let service: AppartenirpromotionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppartenirpromotionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});