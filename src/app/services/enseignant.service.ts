import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Enseignant } from '../model/enseignant.model';
import { Observable, of } from 'rxjs';
import { LogService } from './log.service';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class EnseignantService {

  public host: string = "http://localhost:8080";

  constructor(private httpClient: HttpClient, private logService: LogService) { }

  public getEnseignants(): Observable<Enseignant[]> {
    return this.httpClient.get<Enseignant[]>(this.host + "/listEnseignant")
      .pipe(catchError((error: any): Observable<Enseignant[]> => {
        this.logService.error(error);
        return of(null);
      }));
  }


  public getEnseignantByKeyword(mc: string) {
    return this.httpClient.get(this.host + "listEnseignant/byName?mc=" + mc);
  }


  public deleteEnseignant(id: number) {
    this.httpClient.delete(`${this.host}/deleteEnseignant/${id}`).subscribe(data => {
      console.log(data);
    });
  }

  public saveEnseignant(enseignant: Enseignant): Observable<Enseignant> {

    return this.httpClient.post<Enseignant>(this.host + "/saveEnseignant", enseignant)
      .pipe(catchError((error: any): Observable<Enseignant> => {
        this.logService.error(error);
        return of(new Enseignant);
      }));
  }

  public getResource(url: any): Observable<Enseignant> {
    return this.httpClient.get<Enseignant>(url);
  }

  public updateEnseignant(id: number, enseignant: Enseignant) {

    this.httpClient.put(`${this.host}/updateEnseignant/${id}`, enseignant).subscribe(data => {
      console.log(data);
    });
  }

}
