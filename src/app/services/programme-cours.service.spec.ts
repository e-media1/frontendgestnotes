import { TestBed } from '@angular/core/testing';

import { ProgrammeCoursService } from './programme-cours.service';

describe('ProgrammeCoursService', () => {
  let service: ProgrammeCoursService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProgrammeCoursService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
