import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Evaluation } from '../model/evaluation.model';
import { Matiere } from '../model/matiere.model';
import { ProgrammeCours } from '../model/programmeCours.model';
import { LogService } from './log.service';


@Injectable({
  providedIn: 'root'
})
export class EvaluationService {
  public host: string = "http://localhost:8080";

  constructor(private httpClient: HttpClient, private logService: LogService) { }

  findAll(): Observable<Evaluation> {
    //const cleanedParams = Helpers.cleanHttpParams(params);
    return this.httpClient.get<Evaluation>(`${this.host}/listEvaluation`)
      // params: cleanedParams,
      .pipe(
        catchError(
          (error: any): Observable<Evaluation> => {
            this.logService.error(error);
            return of(null);
          }
        )
      );
  }

  getEvaluations(): Observable<Evaluation[]> {
    return this.httpClient.get<Evaluation[]>(this.host + "/listEvaluation")
      .pipe(catchError((error: any): Observable<Evaluation[]> => {
        this.logService.error(error);
        return of(null);
      }));
  }

  public deleteEvaluation(id: number) {
    this.httpClient.delete(`${this.host}/deleteEvaluation/${id}`).subscribe(data => {
      console.log(data);
    })
  }


  public save(evaluation: Evaluation): Observable<Evaluation> {
    // programmeCours
    // let e = new Evaluation;
    // // e.id = evaluation.id;
    // e.dateEvaluation = evaluation.dateEvaluation;
    // e.titreEvaluation = evaluation.titreEvaluation;
    console.log ('evaluation =',JSON.stringify(evaluation));
    // e.programmeCours = new ProgrammeCours;
    // console.log (evaluation.programme);
    // e.programmeCours.id = evaluation.programmeCours.id;
    // e.programmeCours.coef = evaluation.programmeCours.coef;
    // e.programmeCours.observationCours = evaluation.programmeCours.observationCours;
    // e.programmeCours.dateDebutCours = evaluation.programmeCours.dateDebutCours;
    // e.programmeCours.dateFinCours = evaluation.programmeCours.dateFinCours;
    // e.programmeCours.matiere = new Matiere;
    // // console.log (evaluation.programmeCours.matiere.id);
    // e.programmeCours.matiere.id = evaluation.programmeCours.matiere.id;
    // e.programmeCours.matiere.codeMatiere = evaluation.programmeCours.matiere.codeMatiere;
    // e.programmeCours.matiere.libelleMatiere = evaluation.programmeCours.matiere.libelleMatiere;
    // e.programmeCours.matiere.coefMatiere = evaluation.programmeCours.matiere.coefMatiere;
    // e.programmeCours.matiere.dateDebutMatiere = evaluation.programmeCours.matiere.dateDebutMatiere;
    // e.programmeCours.matiere.dateFinMatiere = evaluation.programmeCours.matiere.dateFinMatiere;

    return this.httpClient.post<Evaluation>(`${this.host}/saveEvaluation`, evaluation)
      .pipe(catchError((error: any): Observable<Evaluation> => {
        this.logService.error(error);
        return of(new Evaluation);
      }));
  }
   public updateEvaluation(id: number, evaluation: Evaluation) {
    this.httpClient.put(`${this.host}/updateEvaluation/${id}`, evaluation).subscribe(data => {
      console.log(data);
    });
  }





}
