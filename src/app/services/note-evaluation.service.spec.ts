import { TestBed } from '@angular/core/testing';

import { NoteEvaluationService } from './note-evaluation.service';

describe('NoteEvaluationService', () => {
  let service: NoteEvaluationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NoteEvaluationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});