import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Typeappreciation } from '../model/typeappreciation.model';

@Injectable({
  providedIn: 'root'
})
export class TypeappreciationService {
  public host:string="http://localhost:8080";
  logService: any;

  constructor(private httpClient:HttpClient) { }

  findAll(): Observable<Typeappreciation> {
    //const cleanedParams = Helpers.cleanHttpParams(params);
    return this.httpClient.get<Typeappreciation>(`${this.host}/apprSanction`)
       // params: cleanedParams,
      .pipe(
        catchError(
          (error: any): Observable<Typeappreciation> => {
            this.logService.error(error);
            return of(null);
          }
        )
      );
  }

  getTypeappreciations(): Observable<Typeappreciation[]> {
    return this.httpClient.get<Typeappreciation[]>(`${this.host}/apprSanction`)
      .pipe(catchError((error: any): Observable<Typeappreciation[]> => {
        this.logService.error(error);
        return of(null);
      }));
  }

  delete(id:number) {
    return this.httpClient.delete(`${this.host}/apprSanction/${id}`)
      .pipe(catchError((error: any): Observable<Typeappreciation> => {
        this.logService.error(error);
        return of(new Typeappreciation);
      }));
  }


  public saveTypeappreciation(typeappreciation:Typeappreciation):Observable<Typeappreciation>{
    return this.httpClient.post<Typeappreciation>(`${this.host}/apprSanction`, typeappreciation)
    .pipe(catchError((error:any):Observable<Typeappreciation> =>{
      this.logService.error(error);
      return of (new Typeappreciation);
    }));
  }

  public getResource(url:any):Observable<Typeappreciation>{
    return this.httpClient.get<Typeappreciation>(url);
  }


  public deleteTypeappreciation(id: number) {
    this.httpClient.delete(`${this.host}/apprSanction/${id}`).subscribe(data => {
      console.log(data);
    });
  }

  public updateTypeappreciation(id: number, typeappreciation:Typeappreciation) {
    this.httpClient.put(`${this.host}/apprSanction/${id}`, typeappreciation).subscribe(data => {
      console.log(data);
    });
  }


}
