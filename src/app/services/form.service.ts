import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgForm, ValidationErrors } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { LogService } from './log.service';


@Injectable({
  providedIn: 'root'
})
export class FormService {
  constructor(private messageService: MessageService,
    private logService: LogService) { }

  isNgFormValid(ngForm: NgForm): any {
    if (ngForm.invalid) {
      let messageDetails: string = "";

      Object.keys(ngForm.form.controls).forEach(key => {

        const controlErrors: ValidationErrors = ngForm.form.get(key).errors;
        this.logService.log(controlErrors);

        if (controlErrors != null) {
          Object.keys(controlErrors).forEach(keyError => {
            if (keyError == "required" && controlErrors[keyError]) {
              messageDetails = " Le champ '" + key + "' est  obligatoire";
              this.messageService.add({
                severity: "error", summary: messageDetails,
                detail: "", life: 5000,
              });
            } else
              if (keyError == "email" && controlErrors[keyError]) {
                messageDetails = " La valeur du champ " + key + " n'est pas un email valide";
                this.messageService.add({
                  severity: "error", summary: messageDetails,
                  detail: "", life: 5000,
                });
              } else
                if (keyError == "pattern" && controlErrors[keyError] && controlErrors[keyError].requiredPattern) {
                  messageDetails = " La valeur du champ " + key + " ne respecte pas le schÃ©ma " + controlErrors[keyError].requiredPattern;
                  this.messageService.add({
                    severity: "error", summary: messageDetails,
                    detail: "", life: 5000,
                  });
                } else
                  if (keyError == "minlength" && controlErrors[keyError] && controlErrors[keyError].requiredLength) {
                    messageDetails = " La valeur du champ " + key + " doit avoir une longueur minimale de " + controlErrors[keyError].requiredLength;
                    this.messageService.add({
                      severity: "error", summary: messageDetails,
                      detail: "", life: 5000,
                    });
                  } else
                    if (keyError == "maxlength" && controlErrors[keyError] && controlErrors[keyError].requiredLength) {
                      messageDetails = " La valeur du champ " + key + " doit avoir une longueur maximale de " + controlErrors[keyError].requiredLength;
                      this.messageService.add({
                        severity: "error", summary: messageDetails,
                        detail: "", life: 5000,
                      });

                    } else {
                      messageDetails = " La valeur du champ " + key + "n' est pas valide " + controlErrors[keyError];
                      this.messageService.add({
                        severity: "error", summary: messageDetails,
                        detail: "", life: 5000,
                      });
                    }

            this.logService.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ');
          });
        }
      });
      return false;

    }

    return true;
  }

}
