import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Session } from '../model/session.model';
import { LogService } from './log.service';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  public host: string = "http://localhost:8080";


  constructor(private httpClient: HttpClient,private logService: LogService) { }

  findAll(): Observable<Session> {
    //const cleanedParams = Helpers.cleanHttpParams(params);
    return this.httpClient.get<Session>(`${this.host}/listSession`)
      // params: cleanedParams,
      .pipe(
        catchError(
          (error: any): Observable<Session> => {
            this.logService.error(error);
            return of(null);
          }
        )
      );
  }

  getSessions(): Observable<Session[]> {
    return this.httpClient.get<Session[]>(`${this.host}/listSession`)
      .pipe(catchError((error: any): Observable<Session[]> => {
        this.logService.error(error);
        return of(null);
      }));
  }


  public deleteSession(id: number) {
    this.httpClient.delete(`${this.host}/deleteSession/${id}`).subscribe(data => {
      console.log(data);
    });
  }


  public saveSession(session:Session):Observable<Session>{

    return this.httpClient.post<Session>(`${this.host}/saveSession`, session)

      .pipe(catchError((error: any): Observable<Session> => {
        this.logService.error(error);
        return of(new Session);
      }));
  }

  public getResource(url: any): Observable<Session> {
    return this.httpClient.get<Session>(url);
  }


  public updateSession(id:number,session: Session): Observable<Session> {
    let ses = new Session;
    ses.id = session.id;
    ses.rangSession = session.rangSession;
    ses.anneeSession = session.anneeSession;
    ses.dateDebutsession = session.dateDebutsession;
    ses.dateFinsession = session.dateFinsession;

    return this.httpClient.put<Session>(`${this.host}/listSession/${id}`, ses)
      .pipe(catchError((error: any): Observable<Session> => {
        this.logService.error(error);
        return of(new Session);
      }));
  }

}
