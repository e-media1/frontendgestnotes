import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Etudiant } from '../model/etudiant.model';

@Injectable({
  providedIn: 'root'
})
export class EtudiantService {
  public host: string = "http://localhost:8080";
  logService: any;

  constructor(private httpClient: HttpClient) { }

  findAll(): Observable<Etudiant> {
    //const cleanedParams = Helpers.cleanHttpParams(params);
    return this.httpClient.get<Etudiant>(`${this.host}/listEtudiant`)
      // params: cleanedParams,
      .pipe(
        catchError(
          (error: any): Observable<Etudiant> => {
            this.logService.error(error);
            return of(null);
          }
        )
      );
  }

  getEtudiants(): Observable<Etudiant[]> {
    return this.httpClient.get<Etudiant[]>(`${this.host}/listEtudiant`)
      .pipe(catchError((error: any): Observable<Etudiant[]> => {
        this.logService.error(error);
        return of(null);
      }));
  }

  public saveEtudiant(etudiant:Etudiant):Observable<Etudiant>{

    return this.httpClient.post<Etudiant>(this.host+"/saveEtudiant", etudiant)
    .pipe(catchError((error:any):Observable<Etudiant> =>{
      this.logService.error(error);
      return of (new Etudiant);
    }));
  }

  public getResource(url: any): Observable<Etudiant> {
    return this.httpClient.get<Etudiant>(url);
  }

  public deleteEtudiant(id: number) {
    this.httpClient.delete(`${this.host}/deleteEtudiant/${id}`).subscribe(data => {
      console.log(data);
    });
  }

  public updateEtudiant(id: number, etudiant:Etudiant) {

    this.httpClient.put(`${this.host}/updateEtudiant/${id}`, etudiant).subscribe(data => {
      console.log(data);
    });
  }
}
