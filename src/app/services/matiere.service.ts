import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Enseignant } from '../model/enseignant.model';
import { Matiere } from '../model/matiere.model';
import { Session } from '../model/session.model';

@Injectable({
  providedIn: 'root'
})
export class MatiereService {
  public host: string = "http://localhost:8080";
  logService: any;

  constructor(private httpClient: HttpClient) { }

  findAll(): Observable<Matiere> {
    //const cleanedParams = Helpers.cleanHttpParams(params);
    return this.httpClient.get<Matiere>(`${this.host}/listMatiere`)
      // params: cleanedParams,
      .pipe(
        catchError(
          (error: any): Observable<Matiere> => {
            this.logService.error(error);
            return of(null);
          }
        )
      );
  }
getMat(){
  this.httpClient.get(`${this.host}/listMatiere`).subscribe(data => {
    console.log(data);
  });
}
  getMatieres(): Observable<Matiere[]> {
    return this.httpClient.get<Matiere[]>(`${this.host}/listMatiere`)
      .pipe(catchError((error: any): Observable<Matiere[]> => {
        this.logService.error(error);
        return of(null);
      }));
  }

  public deleteMatiere(id:number){
    this.httpClient.delete(`${this.host}/deleteMatiere/${id}`).subscribe(data => {
      console.log(data);
    })
  }

  public saveMatiere(matiere: Matiere): Observable<Matiere> {
    let mat = new Matiere;
    mat.codeMatiere = matiere.codeMatiere;
    mat.coefMatiere = matiere.coefMatiere;
    mat.libelleMatiere = matiere.libelleMatiere;
    mat.dateDebutMatiere = matiere.dateDebutMatiere;
    mat.dateFinMatiere = matiere.dateFinMatiere;
    //enseignant
    mat.enseignant = new Enseignant;
    mat.enseignant.id = matiere.enseignant.id;
    mat.enseignant.nom = matiere.enseignant.nom;
    mat.enseignant.prenom = matiere.enseignant.prenom;
    mat.enseignant.adresse = matiere.enseignant.adresse;
    mat.enseignant.matricule = matiere.enseignant.matricule;
    mat.enseignant.email = matiere.enseignant.email;
    mat.enseignant.pays = matiere.enseignant.pays;
    mat.enseignant.telephone = matiere.enseignant.telephone;
    mat.enseignant.ville = matiere.enseignant.ville;
    mat.enseignant.date_recrut = matiere.enseignant.date_recrut;
    //sessions
    mat.session = new Session;
    mat.session.id = matiere.session.id;
    mat.session.rangSession = matiere.session.rangSession;
    mat.session.anneeSession = matiere.session.anneeSession;
    mat.session.dateDebutsession = matiere.session.dateDebutsession;
    mat.session.dateFinsession = matiere.session.dateFinsession;

    return this.httpClient.post<Matiere>(`${this.host}/saveMatiere`, mat)
      .pipe(catchError((error: any): Observable<Matiere> => {
        this.logService.error(error);
        return of(new Matiere);
      }));
  }

  public getResource(url: any): Observable<Matiere> {
    return this.httpClient.get<Matiere>(url);
  }



  public updateMatiere(id:number,matiere: Matiere){
    let mat = new Matiere;
    mat.codeMatiere = matiere.codeMatiere;
    mat.coefMatiere = matiere.coefMatiere;
    mat.libelleMatiere = matiere.libelleMatiere;
    mat.dateDebutMatiere = matiere.dateDebutMatiere;
    mat.dateFinMatiere = matiere.dateFinMatiere;
    //enseignant
    mat.enseignant = new Enseignant;
    mat.enseignant.id = matiere.enseignant.id;
    mat.enseignant.nom = matiere.enseignant.nom;
    mat.enseignant.prenom = matiere.enseignant.prenom;
    mat.enseignant.adresse = matiere.enseignant.adresse;
    mat.enseignant.matricule = matiere.enseignant.matricule;
    mat.enseignant.email = matiere.enseignant.email;
    mat.enseignant.pays = matiere.enseignant.pays;
    mat.enseignant.telephone = matiere.enseignant.telephone;
    mat.enseignant.ville = matiere.enseignant.ville;
    mat.enseignant.date_recrut = matiere.enseignant.date_recrut;
    //sessions
    mat.session = new Session;
    mat.session.id = matiere.session.id;
    mat.session.rangSession = matiere.session.rangSession;
    mat.session.anneeSession = matiere.session.anneeSession;
    mat.session.dateDebutsession = matiere.session.dateDebutsession;
    mat.session.dateFinsession = matiere.session.dateFinsession;

    this.httpClient.put(`${this.host}/updateMatiere/${id}`, mat).subscribe(data => {
      console.log(data);
    });
  }



  }


