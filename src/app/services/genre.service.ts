import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';




@Injectable({
  providedIn: 'root'
})
export class GenreService {

  genres: any[] = [
    { name: 'Masculin', code: 'Masculin' },
    { name: 'Feminin', code: 'Feminin' }
  ];

  list(): any[] {
    return this.genres;
  }

}
