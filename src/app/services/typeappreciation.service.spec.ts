import { TestBed } from '@angular/core/testing';

import { TypeappreciationService } from './typeappreciation.service';

describe('TypeappreciationService', () => {
  let service: TypeappreciationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TypeappreciationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
