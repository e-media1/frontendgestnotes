import { HttpClient,HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Helpers } from '../model/helpers';
import { NoteEvaluation } from '../model/note-evaluation.model';
import { LogService } from './log.service';


@Injectable({
  providedIn: 'root'
})
export class ReleveService {
public host = "http://localhost:8080";
public resultats:any;
  constructor(private httpClient:HttpClient,
    private logService: LogService) {

   }



  affiche(idEtudiant:number): Observable<NoteEvaluation[]> {
    const params = new HttpParams()
      .set('idEtudiant', idEtudiant);
        return this.httpClient.get<NoteEvaluation[]>(`${this.host}/listNoteEvaluation`,{params});
      }
   

  afficher(idEtudiant:number,idSessionpromotion:number): Observable<NoteEvaluation[]> {
const params = new HttpParams()
  .set('idEtudiant', idEtudiant)
  .set('idsp', idSessionpromotion);
    return this.httpClient.get<NoteEvaluation[]>(`${this.host}/listNoteEvaluation/searchetudiant`,{params});
  }

   search(params: any = {}): Observable<NoteEvaluation> {
    const cleanedParams = Helpers.cleanHttpParams(params);
    return this.httpClient.get<NoteEvaluation>(`${this.host}/search`, {
      params: cleanedParams,
    })
      .pipe(
        catchError(
          (error: any): Observable<NoteEvaluation> => {
            this.logService.error(error);
            return of(null);
          }
        )
      );
  }
}
