import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Promotion } from '../model/promotion.model';
import { Session } from '../model/session.model';
import { Sessionpromotion } from '../model/sessionpromotion.model';

@Injectable({
  providedIn: 'root'
})
export class SessionpromotionService {
  public host: string = "http://localhost:8080";
  logService: any;

  constructor(private httpClient: HttpClient) { }

  findAll(): Observable<Sessionpromotion> {
    //const cleanedParams = Helpers.cleanHttpParams(params);
    return this.httpClient.get<Sessionpromotion>(`${this.host}/listSession`)
      // params: cleanedParams,
      .pipe(
        catchError(
          (error: any): Observable<Sessionpromotion> => {
            this.logService.error(error);
            return of(null);
          }
        )
      );
  }

  getSessionpromotions(): Observable<Sessionpromotion[]> {
    return this.httpClient.get<Sessionpromotion[]>(`${this.host}/listSessionsPromotions`)
      .pipe(catchError((error: any): Observable<Sessionpromotion[]> => {
        this.logService.error(error);
        return of(null);
      }));
  }

  public deleteSessionpromotion(id: number) {
    this.httpClient.delete(`${this.host}/deleteSessionsPromotions/${id}`).subscribe(data => {
      console.log(data);
    })
  }


  public saveSessionpromotion(sessionpromotion: Sessionpromotion): Observable<Sessionpromotion> {
    let sp = new Sessionpromotion;
    sp.id = sessionpromotion.id;
    sp.rang = sessionpromotion.rang;
    //promotion
    sp.promotion = new Promotion;
    sp.promotion.id = sessionpromotion.promotion.id;
    sp.promotion.nom = sessionpromotion.promotion.nom;
    sp.promotion.date_debut = sessionpromotion.promotion.date_debut;
    sp.promotion.date_fin = sessionpromotion.promotion.date_fin;
    //session
    sp.session = new Session;
    sp.session.id = sessionpromotion.session.id;
    sp.session.anneeSession = sessionpromotion.session.anneeSession;
    sp.session.rangSession = sessionpromotion.session.rangSession;
    sp.session.dateDebutsession = sessionpromotion.session.dateDebutsession;
    sp.session.dateFinsession = sessionpromotion.session.dateFinsession;

    return this.httpClient.post<Sessionpromotion>(`${this.host}/saveSessionsPromotions`, sp)
      .pipe(catchError((error: any): Observable<Sessionpromotion> => {
        this.logService.error(error);
        return of(new Sessionpromotion);
      }));
  }

  public getResource(url: any): Observable<Sessionpromotion> {
    return this.httpClient.get<Sessionpromotion>(url);
  }



  public updateSessionpromotion(id: number, sessionpromotion: Sessionpromotion) {

    this.httpClient.put(`${this.host}/updateSessionsPromotions/${id}`, sessionpromotion)
    .subscribe(data => {
      console.log(data);
    });
  }


}
