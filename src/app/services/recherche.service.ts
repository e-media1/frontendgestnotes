import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RechercheService {
public host = "http://localhost:8080";
public resultats:any;
  constructor(private httpClient:HttpClient) {

   }

   public rechercheentrer(term1:any,term2:any):Observable<any>{
     let params ={p1:term1,p2:term2}
return this.httpClient.get(this.host+"/listNoteEvaluation",{params}).pipe(
  map(response =>{
    console.log(response);
    return this.resultats = response["items"];
  })
);
   }

   public _rechercheentrer(term1,term2){
     return this.rechercheentrer(term1,term2);
   }
}
