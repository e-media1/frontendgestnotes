import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
//import { Session } from 'inspector';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Appartenirpromotion } from '../model/appartenirpromotion.model';
import { Etudiant } from '../model/etudiant.model';
import { Promotion } from '../model/promotion.model';
//import { Session } from '../model/session.model';

@Injectable({
  providedIn: 'root'
})
export class AppartenirpromotionService {
  public host: string = "http://localhost:8080";
  logService: any;

  constructor(private httpClient: HttpClient) { }

  findAll(): Observable<Appartenirpromotion> {
    //const cleanedParams = Helpers.cleanHttpParams(params);
    return this.httpClient.get<Appartenirpromotion>(`${this.host}/listApprPromo`)
      // params: cleanedParams,
      .pipe(
        catchError(
          (error: any): Observable<Appartenirpromotion> => {
            this.logService.error(error);
            return of(null);
          }
        )
      );
  }

  getAppartenirpromotion(): Observable<Appartenirpromotion[]> {
    return this.httpClient.get<Appartenirpromotion[]>(`${this.host}/listApprPromo`)
      .pipe(catchError((error: any): Observable<Appartenirpromotion[]> => {
        this.logService.error(error);
        return of(null);
      }));
  }


  public getResource(url: any): Observable<Appartenirpromotion> {
    return this.httpClient.get<Appartenirpromotion>(url);
  }

  public deleteAppartenirpromotion(id: number) {
    this.httpClient.delete(`${this.host}/listApprPromo/${id}}`).subscribe(data => {
      console.log(data);
    });
  }

  public updateAppartenirpromotion(id: number,appartenirpromotion: Appartenirpromotion) {
    let app = new Appartenirpromotion;
    app.id = appartenirpromotion.id;
    app.dateEntree = appartenirpromotion.dateEntree;
    app.exclu = appartenirpromotion.exclu;
    app.motifExclusion = appartenirpromotion.motifExclusion;
    app.dateExclusion = appartenirpromotion.dateExclusion;
    app.dernierSession = appartenirpromotion.dernierSession;

    //etudiant
    app.etudiant = new Etudiant;
    app.etudiant.id = appartenirpromotion.etudiant.id;
    app.etudiant.nomEtudiant = appartenirpromotion.etudiant.nomEtudiant;
    app.etudiant.prenomEtudiant = appartenirpromotion.etudiant.prenomEtudiant;
    app.etudiant.sexeEtudiant = appartenirpromotion.etudiant.sexeEtudiant;
    app.etudiant.dateNaissanceEtudiant = appartenirpromotion.etudiant.dateNaissanceEtudiant;
    app.etudiant.lieuNaissanceEtudiant = appartenirpromotion.etudiant.lieuNaissanceEtudiant;
    app.etudiant.nationaliteEtudiant = appartenirpromotion.etudiant.nationaliteEtudiant;
    app.etudiant.emailEtudiant = appartenirpromotion.etudiant.emailEtudiant;
    app.etudiant.telephoneEtudiant = appartenirpromotion.etudiant.telephoneEtudiant;
    app.etudiant.adresseEtudiant = appartenirpromotion.etudiant.adresseEtudiant;
    app.etudiant.villeEtudiant = appartenirpromotion.etudiant.villeEtudiant;
    app.etudiant.paysEtudiant = appartenirpromotion.etudiant.paysEtudiant;
    app.etudiant.numeroCNI = appartenirpromotion.etudiant.numeroCNI;
    app.etudiant.dateInscription = appartenirpromotion.etudiant.dateInscription;

    //Promotion
    app.promotion = new Promotion;
    app.promotion.id = appartenirpromotion.promotion.id;
    app.promotion.nom = appartenirpromotion.promotion.nom;
    app.promotion.date_debut = appartenirpromotion.promotion.date_debut;
    app.promotion.date_fin = appartenirpromotion.promotion.date_fin;


    this.httpClient.put(`${this.host}/listApprPromo/${id}`, app).subscribe(data => {
      console.log(data);
    });
  }

  public saveAppartenirpromotion(appartenirpromotion: Appartenirpromotion): Observable<Appartenirpromotion> {
    let app = new Appartenirpromotion;
    app.dateEntree = appartenirpromotion.dateEntree;
    app.exclu = appartenirpromotion.exclu;
    app.motifExclusion = appartenirpromotion.motifExclusion;
    app.dateExclusion = appartenirpromotion.dateExclusion;
    app.dernierSession = appartenirpromotion.dernierSession;
    //etudiant
    app.etudiant = new Etudiant;
    app.etudiant.id = appartenirpromotion.etudiant.id;
    app.etudiant.nomEtudiant = appartenirpromotion.etudiant.nomEtudiant;
    app.etudiant.prenomEtudiant = appartenirpromotion.etudiant.prenomEtudiant;
    app.etudiant.sexeEtudiant = appartenirpromotion.etudiant.sexeEtudiant;
    app.etudiant.dateNaissanceEtudiant = appartenirpromotion.etudiant.dateNaissanceEtudiant;
    app.etudiant.lieuNaissanceEtudiant = appartenirpromotion.etudiant.lieuNaissanceEtudiant;
    app.etudiant.nationaliteEtudiant = appartenirpromotion.etudiant.nationaliteEtudiant;
    app.etudiant.emailEtudiant = appartenirpromotion.etudiant.emailEtudiant;
    app.etudiant.telephoneEtudiant = appartenirpromotion.etudiant.telephoneEtudiant;
    app.etudiant.adresseEtudiant = appartenirpromotion.etudiant.adresseEtudiant;
    app.etudiant.villeEtudiant = appartenirpromotion.etudiant.villeEtudiant;
    app.etudiant.paysEtudiant = appartenirpromotion.etudiant.paysEtudiant;
    app.etudiant.numeroCNI = appartenirpromotion.etudiant.numeroCNI;
    app.etudiant.dateInscription = appartenirpromotion.etudiant.dateInscription;

    //Promotion
    app.promotion = new Promotion;
    app.promotion.id = appartenirpromotion.promotion.id;
    app.promotion.nom = appartenirpromotion.promotion.nom;
    app.promotion.date_debut = appartenirpromotion.promotion.date_debut;
    app.promotion.date_fin = appartenirpromotion.promotion.date_fin;



    return this.httpClient.post<Appartenirpromotion>(`${this.host}/listApprPromo`, app)
      .pipe(catchError((error: any): Observable<Appartenirpromotion> => {
        this.logService.error(error);
        return of(new Appartenirpromotion);
      }));

  }
}
